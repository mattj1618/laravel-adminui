@extends('admin-ui::layout.login')

@section('header')
    @if(Config::get('admin-ui.logo'))
        <img src="https://cdn.communitytogo.com.au/images/c2go.png"/>
    @else
        {{ Config::get('admin-ui.title') }}
    @endif
@stop


@section('content')

    <p class="login-box-msg">Sign in to get started!</p>
    <form action="{{ isset($actionUrl) ? $actionUrl : '/login' }}" method="post">
        {{ isset($message) ? $message : '' }}
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Email"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
        </div>
    </form>

@stop