

    <header class="main-header">
        @if(Config::get('admin-ui.header-image')===null)
            <a href="/" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <b>{{ Config::get('admin-ui.name1') }}</b>{{ Config::get('admin-ui.name2') }}
            </a>
        @else

            <a href="/" class="logo" style="
                background-color: white;
                padding: 2px;
            ">
                <div style="background: {{Config::get('admin-ui.header-colour')}} url({{Config::get('admin-ui.header-image')}}) no-repeat center center;background-size:contain;width:100%;height:100%;"></div>
            </a>
        @endif
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation" id="top-navbar">
<!-- Sidebar toggle button-->
<a href="#" class="sidebar-toggle" id="menu-button" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-custom-menu">
<ul class="nav navbar-nav">

    @if(Auth::check())
        <li class=" ">
            <a href="/logout">
                <span>Logout</span>
            </a>
        </li>
    @else
        <li class=" ">
            <a href="/login">
                <span>Login</span>
            </a>
        </li>
    @endif

</ul>
</div>
</nav>
</header>