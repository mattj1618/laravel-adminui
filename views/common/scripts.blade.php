
@if(Config::get('admin-ui.stripe'))
    {{--<script src="https://checkout.stripe.com/checkout.js" type="text/javascript"></script>--}}
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://js.stripe.com/v1/"></script>
@endif



<link href="https://cdn.communitytogo.com.au/lte2.02/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.communitytogo.com.au/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
{{--##<link href="https://cdn.communitytogo.com.au/ionicons-1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />--}}


<!-- Ionicons -->
<link href="https://cdn.communitytogo.com.au/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

<link href="https://cdn.communitytogo.com.au/lte2.02/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

<link href="https://cdn.communitytogo.com.au/lte2.02/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

@if(Config::get('admin-ui.colour')!==null)
    <link href="https://cdn.communitytogo.com.au/lte2.02/dist/css/skins/skin-{{Config::get('admin-ui.colour')}}.min.css" rel="stylesheet" type="text/css" />
@endif


<!-- iCheck for checkboxes and radio inputs -->
{{--<link href="https://cdn.communitytogo.com.au/lte2.02/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />--}}


<!-- daterange picker -->
<link href="https://cdn.communitytogo.com.au/lte2.02/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

<style>
    .color-palette {
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
    .color-palette-set {
        margin-bottom: 15px;
    }
    .color-palette span {
        display: none;
    }
    .color-palette:hover span {
        display: block;
    }
    pre {
        overflow: auto;
        word-wrap: normal;
        white-space: pre;
        white-space: pre-wrap;       /* css-3 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word;       /* Internet Explorer 5.5+ */
        font-family:'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        background:inherit;
        color: inherit;
        border:0px;
        font-size:inherit;
    }
</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="https://cdn.communitytogo.com.au/jquery-1.11.1/jquery.min.js" type="text/javascript"></script>

<script src="https://cdn.communitytogo.com.au/jconfirm/jquery.confirm.min.js" type="text/javascript"></script>
<script src="https://cdn.communitytogo.com.au/jqcookie/jquery.cookie.js" type="text/javascript"></script>


<script src="https://cdn.communitytogo.com.au/lte2.02/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="https://cdn.communitytogo.com.au/lte2.02/dist/js/app.min.js" type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
{{--##<script src="https://cdn.communitytogo.com.au/lte2.02/js/AdminLTE/demo.js" type="text/javascript"></script>--}}

<link href="https://cdn.communitytogo.com.au/DataTables-1.10.5/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.communitytogo.com.au/DataTables-1.10.5/media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.communitytogo.com.au/DataTables-1.10.5/media/js/jquery.dataTables.min.js" type="text/javascript"></script>


<script src="https://cdn.communitytogo.com.au/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>


{{--##<script type="text/javascript" src="/js/minimo.js" type="text/javascript"></script>--}}
{{--##<link href="/css/minimo.css" rel="stylesheet" type="text/css" />--}}


{{--<link href='/plugins/jquery-TE/jquery-te-1.4.0.css' rel='stylesheet' type='text/css'>--}}
{{--<script src="https://cdn.communitytogo.com.au/jquery-TE/jquery-te-1.4.0.min.js" type="text/javascript"></script>--}}

{{--<script type="text/javascript">--}}
    {{--$(document).ready(function(){--}}
        {{--$(".teeditor").jqte({format: false});--}}

        {{--//iCheck for checkbox and radio inputs--}}
        {{--$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({--}}
            {{--checkboxClass: 'icheckbox_minimal-blue',--}}
            {{--radioClass: 'iradio_minimal-blue'--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}

<!-- iCheck 1.0.1 -->
{{--<script src="https://cdn.communitytogo.com.au/lte2.02/plugins/iCheck/icheck.min.js" type="text/javascript"></script>--}}

{{--##<link href="https://cdn.communitytogo.com.au/DataTables-1.10.5/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />--}}
{{--##<link href="https://cdn.communitytogo.com.au/DataTables-1.10.5/media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />--}}
{{--##<script src="https://cdn.communitytogo.com.au/DataTables-1.10.5/media/js/jquery.dataTables.min.js" type="text/javascript"></script>--}}
{{--##<script src="https://cdn.communitytogo.com.au/bootstrap-list-filter/bootstrap-list-filter.src.js" type="application/javascript"/>--}}


<link href="https://cdn.communitytogo.com.au/datepicker/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.communitytogo.com.au/datepicker/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.communitytogo.com.au/datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

<link href="https://cdn.communitytogo.com.au/select2/select2.min.css" rel="stylesheet" />
<script src="https://cdn.communitytogo.com.au/select2/select2.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2();
    });
</script>
<!-- date-range-picker -->
<script src="https://cdn.communitytogo.com.au/moment/moment.js" type="text/javascript"></script>
<script src="https://cdn.communitytogo.com.au/lte2.02/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">

<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false
        });
    });
</script>

<script src="https://cdn.communitytogo.com.au/tinymce/js/tinymce/jquery.tinymce.min.js" type="text/javascript"></script>
<script src="https://cdn.communitytogo.com.au/tinymce/js/tinymce/tinymce.min.js" type="text/javascript"></script>

<!-- colorbox -->
    <script src="https://cdn.communitytogo.com.au/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>
    <link href="https://cdn.communitytogo.com.au/colorbox/colorbox.css" rel="stylesheet"/>



    <!-- Bootstrap time Picker -->
<link href="https://cdn.communitytogo.com.au/lte2.02/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>

<!-- bootstrap time picker -->
<script src="https://cdn.communitytogo.com.au/lte2.02/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>

<style type="text/css">
    label {
        font-size: 18px;
    }
</style>

<!-- Progress -->
<link href="https://cdn.communitytogo.com.au/progtrckr/progtrckr.css" rel="stylesheet"/>
<script src="https://cdn.communitytogo.com.au/progtrckr/progtrckr.js" type="text/javascript"></script>


<link href="https://cdn.communitytogo.com.au/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet" type="text/css" />


<!-- bootstrap switch -->
<link href="https://cdn.communitytogo.com.au/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.communitytogo.com.au/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
