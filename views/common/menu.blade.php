
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            @if( \AdminUI::$menuGenerator )
                @foreach( \AdminUI::$menuGenerator->getMenu() as $menu )

                    <ul class="sidebar-menu">
                        <li class="header">{{ $menu['header'] }}</li>
                    </ul>

                    @if(Config::get('admin-ui.menu-collapsed')===true)
                        <ul class="sidebar-menu">
                    @endif

                    @foreach( $menu['items'] as $item )


                        @if( array_key_exists('subItems',$item ))

                            @if(Config::get('admin-ui.menu-collapsed')!==true)
                                <ul class="sidebar-menu">
                            @endif
                                    <li class="treeview
                                        @if(Config::get('admin-ui.menu-collapsed')!==true)
                                            active
                                        @endif
                                        @foreach($item['subItems'] as $subItem)
                                            @if(strpos("/".Request::path(), $subItem['url'])!==false)
                                                active
                                            @endif
                                        @endforeach
                                            ">
                                        <a href="#">
                                            <i class="fa fa-table"></i>
                                            <span>{{$item['name']}}</span>
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </a>
                                        <ul class="treeview-menu

                                            @if(Config::get('admin-ui.menu-collapsed')!==true)
                                                menu-open
                                            @endif

                                            @foreach($item['subItems'] as $subItem)
                                                @if(strpos("/".Request::path(), $subItem['url'])!==false)
                                                    menu-open
                                                @endif
                                            @endforeach

                                            "
                                            @if(Config::get('admin-ui.menu-collapsed')!==true)
                                                style="display:block"
                                            @endif>

                                            @foreach($item['subItems'] as $subItem)
                                                <li><a href="{{ $subItem['url'] }}"><i class="fa fa-angle-double-right"></i> {{ $subItem['name'] }}</a></li>
                                            @endforeach

                                        </ul>
                                    </li>
                            @if(Config::get('admin-ui.menu-collapsed')!==true)
                                </ul>
                            @endif

                        @else
                            @if(Config::get('admin-ui.menu-collapsed')!==true)
                                <ul class="sidebar-menu">
                            @endif
                                    <li>
                                        <a href="{{$item['url']}}">
                                            <i class="fa fa-table"></i> <span>{{ $item['name'] }}</span>
                                        </a>
                                    </li>
                            @if(Config::get('admin-ui.menu-collapsed')!==true)
                                </ul>
                            @endif
                        @endif

                    @endforeach

                    @if(Config::get('admin-ui.menu-collapsed')===true)
                        </ul>
                    @endif
                @endforeach
            @endif
        </section>
        <!-- /.sidebar -->
    </aside>

