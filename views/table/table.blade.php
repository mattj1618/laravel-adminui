{{--##    $attribtues $attr.getId() $attr.getTitle()--}}
{{--##    $ajaxUrl--}}
{{--##    {{ $idAttribute }} (has :{{ $idAttribute }} where id should go)--}}
{{--##    $buttons - $btn.getUrl() $btn.get("text") $btn.getId()--}}
{{--##    $deleteButton = true--}}
{{--##    $header--}}

<script type="text/javascript">
    $(document).ready(function() {
        var table=$('#table-{{ $tableName }}').DataTable( {
            "ajax": "{{$ajaxUrl}}",
            "columns": [
                    @foreach($attributes as $attr)
                        { "data": "{{$attr['id']}}","defaultContent":"" },

                    @endforeach
            ]
        } );

//        single selection
        $('#table-{{ $tableName }} tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
                $('div[table="{{$tableName}}"] .requiresSelect').addClass('disabled');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                $('div[table="{{$tableName}}"] .requiresSelect').removeClass('disabled');
            }
        } );


//        delete row with a confirm box
        $("#deletebtn-{{ $tableName }}").confirm({
            text: "Are you sure you want to delete selected row?",
            title: "Confirmation required",
            confirm: function(button) {
//                table.row('.selected').remove().draw( false );
                if(table.row('.selected').length==1) {
                    console.log("delete path:"+'{{$ajaxDeletePartialUrl}}'.replace("{id}",table.row('.selected').data().id));
                    $.ajax({
                        url: '{{$ajaxDeletePartialUrl}}'.replace("{id}",table.row('.selected').data().id),
                        type: 'DELETE',
                        success: function (result) {
                            console.log('deleted:'+result);
                            if(result=="false"){
                                $('#errorMessage').append('<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b>Alert!</b> Unable to delete this row, it may be that this row is being referenced elsewhere.</div>');
                            } else {
                                table.row('.selected').remove().draw(false);
                                $('table[table="data-table"]').DataTable().ajax.reload(null,false);
                            }

                        }
                    });
                }

            },
            cancel: function(button) {
                // do something
            },
            confirmButton: "Yes",
            cancelButton: "No",
            post: true
        });

        @foreach($buttons as $btn)
            $('#{{ str_replace(' ','-',$btn['id']) }}').click(function(){
                if( {{$btn['requiresSelect']}} && table.row('.selected').length==1)
                {
                    @if(!isset($btn["newTab"]))
                        window.location.href = '{{$btn['url']}}'.replace("{id}", table.row('.selected').data().id);
                    @else
                        window.open('{{$btn['url']}}'.replace("{id}", table.row('.selected').data().id));
                    @endif
                }
                else if({{$btn['requiresSelect']}}==false)
                {
                    @if(!isset($btn["newTab"]))
                        window.location.href = '{{$btn['url']}}';
                    @else
                        window.open('{{$btn['url']}}');
                    @endif
                }
            });
        @endforeach


        //get filtered rows: $("#table-{{ $tableName }}").dataTable()._('tr', {"filter":"applied"});
    } );


</script>

        <div table="{{$tableName}}" style="height:50px;margin-bottom:20px;">
            @if(isset($deleteButton) && $deleteButton === true)
                <button id="deletebtn-{{ $tableName }}" class="btn btn-{{Config::get('admin-ui.default-btn-type')}} requiresSelect disabled " style="float:right;min-width:84px;margin-left:10px;">Delete</button>
            @endif
            @foreach($buttons as $btn)
                @if(isset($btn['float']))
                    <button id="{{str_replace(' ','-',$btn['id'])}}" table="{{$tableName}}" class="btn btn-{{Config::get('admin-ui.default-btn-type')}} @if($btn['requiresSelect']==='true')requiresSelect disabled @endif " style="float:{{$btn['float']}};min-width:84px;margin-left:10px;">{{$btn['text']}}</button>
                @else
                    <button id="{{str_replace(' ','-',$btn['id'])}}" table="{{$tableName}}" class="btn btn-{{Config::get('admin-ui.default-btn-type')}} @if($btn['requiresSelect']==='true')requiresSelect disabled @endif" style="float:right;min-width:84px;margin-left:10px;">{{$btn['text']}}</button>
                @endif
            @endforeach
        </div>

        <div id="errorMessage"></div>

        <table table="data-table" id="table-{{ $tableName }}" class="display table table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                @foreach($attributes as $attr)
                    <th>{{$attr['title']}}</th>
                @endforeach
            </tr>
            </thead>

            <tfoot>
            <tr>
                @foreach($attributes as $attr)
                    <th>{{$attr['title']}}</th>
                @endforeach
            </tr>
            </tfoot>
        </table>
