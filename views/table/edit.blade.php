
                <form id="{{ $form['Form ID'] }}" action="{{ $actionUrl }}" method="POST"  enctype="multipart/form-data" class="form-horizontal">

                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    @if(isset($form['Submit Button Text']))
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<div class="form-group">--}}
                                    <input type="submit" class="btn btn-{{Config::get('admin-ui.default-btn-type')}}" value="{{ $form['Submit Button Text'] }}" />
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    @endif
                    {{--<div class="row"  style="padding-right:20px;padding-left:20px;">--}}
                            @if(isset($form['Inputs']))
                                @foreach($form['Inputs'] as $i)
                                    @if($i->doRender())
                                            {!! $i->render() !!}
                                    @endif
                                @endforeach
                            @endif
                    {{--</div>--}}
                    @if(isset($form['Submit Button Text']))
                        {{--<div class="row">--}}
                        {{--<div class="col-lg-12">--}}
                        {{--<div class="form-group">--}}
                        <input type="submit" class="btn btn-{{Config::get('admin-ui.default-btn-type')}}" value="{{ $form['Submit Button Text'] }}" />
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    @endif
                </form>