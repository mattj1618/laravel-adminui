@extends('admin-ui::layout.print')

@section('content')
    @if(isset($views))
        @foreach($views as $view)
            {!! $view->render() !!}
        @endforeach
    @endif
@stop