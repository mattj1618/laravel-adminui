@extends('admin-ui::layout.login')

@section('header')
    @if(Config::get('admin-ui.logo'))
        <img src="https://cdn.communitytogo.com.au/images/c2go.png"/>
    @else
        {{ Config::get('admin-ui.title') }}
    @endif
@stop


@section('content')

    <p class="login-box-msg">
        {{ $header }}
    </p>

    @if(isset($views))
        @foreach($views as $view)
            {!! $view->render() !!}
        @endforeach
    @endif

@stop