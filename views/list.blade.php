
    <div class="form-group">
        <input id="{{ $list["id"] }}-input" class="form-control" type="search" placeholder="Search..." />
    </div>

    <ul class="products-list product-list-in-box" id="{{ $list["id"] }}-list">

        @foreach($list["items"] as $item)

            <li class="item ">
                <a href="{{$item["href"]}}">

                    @if(isset($item["img"]))
                        <div class="product-img">
                            <img src="{{$item["img"]}}" alt="Image"/>
                        </div>
                    @else
                        <div class="product-img">
                            <span style="font-size: 36px;color:#888">
                                <i class="icon ion-clipboard" data-pack="default" data-tags="write"></i>
                            </span>

                        </div>
                    @endif

                    <div class="product-info">
                        {{ $item["title"] }}
                        <span class="label label-warning pull-right">{{ isset($item["label1"]) ? $item["label1"]:'' }}</span>
                        <span class="product-description">{!! $item["description"] !!}</span>
                        <span class="label label-success pull-right">{{ isset($item["label2"]) ? $item["label2"]:'' }}</span>
                    </div>
                </a>
            </li><!-- /.item -->


        @endforeach

    </ul>


<script>
    $(document).ready(function () {

        (function ($) {

            var id = '{{ $list["id"] }}';

            $('#'+id+'-input').keyup(function () {

                var rex = new RegExp($(this).val(), 'i');
                $('#'+id+'-list li').hide();
                $('#'+id+'-list li').filter(function () {
                    return rex.test($(this).text());
                }).show();

            })

        }(jQuery));

    });
</script>


