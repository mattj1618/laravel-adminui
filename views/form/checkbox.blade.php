
<script type="text/javascript">
    // jQuery OnLoad
    $(function(){

        $('input[name="'+name+'"]').attr('value',$('input[id="'+name+'"]').is(':checked')?'true':'false');

        var name="{{ $item->id }}";
        $('input[id="'+name+'"]').on('change',function(){

            console.log($('input[id="'+name+'"]').is(':checked'));
            $('input[name="'+name+'"]').attr('value',$('input[id="'+name+'"]').is(':checked')?'true':'false');

        });
    });
</script>

<div class="form-group checkbox">

    <input type="checkbox" id="{{ $item->id }}" sub="false" value="true" @if($item->defaultValue) checked @endif />

    <label for="{{ $item->id }}" class="col-sm-10 col-sm-push-2">
        {{ $item->label }}
    </label>

    <div style="display:none" >
        <input type="hidden" name="{{ $item->id }}" @if($item->defaultValue) value="true" @else value="false" @endif  />
    </div>
</div>
