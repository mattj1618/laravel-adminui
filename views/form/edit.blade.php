<div style="padding-bottom:20px;">
@if(isset($form['Progress']))
    @include('admin-ui::form/progress', ['progress'=>$form['Progress']])
@endif
</div>
@if(isset($form['Info Messages']))
    @foreach($form['Info Messages'] as $message)
        {{--<div class="alert alert-info alert-dismissable">--}}
        {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
        {{--<h4><i class="icon fa fa-info"></i> Info!</h4>--}}
        <h4>

            {{ $message }}
        </h4>
        {{--</div>--}}
    @endforeach

@endif

@if(isset($form['Success Messages']))
    @foreach($form['Success Messages'] as $message)

        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4>	<i class="icon fa fa-check"></i> Success!</h4>
            {{ $message }}
        </div>
    @endforeach
@endif

@if(isset($form['Error Messages']))
    @foreach($form['Error Messages'] as $message)
        <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-warning"></i> Warning!</h4>

            {{ $message }}
        </div>
    @endforeach

@endif

{{--{{ Form::open(array('url'=>$actionUrl,'method'=>'POST', 'files'=>true ,'id'=>$form['Form ID'])) }}--}}
                <form id="{{ $form['Form ID'] }}" action="{{ $actionUrl }}" method="POST" enctype="multipart/form-data" class="form-horizontal">

                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    {{--<div class="row"  style="padding-right:20px;padding-left:20px;">--}}
                        @if(isset($form['Inputs']))
                            @foreach($form['Inputs'] as $i)
                                @if($i->doRender())
                                    {!! $i->render() !!}
                                @endif
                            @endforeach
                        @endif
                    {{--</div>--}}
                    @if(isset($form['Submit Button Text']))
                        {{--<div class="row">--}}
                        {{--<div class="col-lg-12">--}}
                        {{--<div class="form-group">--}}
                        <input type="submit" class="btn btn-{{Config::get('admin-ui.default-btn-type')}}" value="{{ $form['Submit Button Text'] }}" />
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    @endif

                    <script type="text/javascript">

                        $('form').submit(function(e) {
                            $('input[type="submit"]').prop('disabled', true);
                        });


                    </script>

                </form>
