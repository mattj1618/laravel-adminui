
<script type="text/javascript">
    // jQuery OnLoad
    $(function(){

        $('input[name="'+name+'"]').attr('value',$('input[name="'+name+'"]').attr('value'));

        var name="{{ $item->id }}";

        $('input[id="'+name+'"]').bootstrapSwitch(
                {
                    "onText":"{{ $item->on }}",
                    "offText":"{{ $item->off }}",
                    onSwitchChange: function(event, state){
                        $('input[name="'+name+'"]').attr('value',state);
                    }
                });
    });
</script>

<div class="form-group col-lg-12">
    <label class="control-label col-sm-2" for="{{ $item->id }}">
        {{ $item->label }}
    </label><br/>


    <div class="col-sm-10">

        <input type="checkbox" id="{{ $item->id }}" sub="false" value="true" @if($item->defaultValue) checked @endif />

        <div style="display:none" >
            <input type="hidden" name="{{ $item->id }}" @if($item->defaultValue) value="true" @else value="false" @endif  />
        </div>
    </div>
</div>
