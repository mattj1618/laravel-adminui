
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>

        <div class="col-sm-10">
            <select id="{{ $item->id }}" name="{{ $item->id }}" class="form-control " multiple="multiple">
                @foreach($item->values as $v)
                    <option value="{{ $v }}"
                            @if($item->isDefault($v))
                                selected
                            @endif>
                        {{ $v }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>