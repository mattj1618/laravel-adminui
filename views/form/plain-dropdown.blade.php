<div class="form-group">
    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>

    <div class="col-sm-10">
        <select name="{{ $item->id }}" class="form-control">

            @if($item->nullable===true)
                <option value="none">
                    None
                </option>
            @endif

            @foreach($item->rows as $row)
                <option value="{{ $row[$item->idField] }}"
                        @if( $item->isDefault( $row[$item->idField] ))
                            selected
                        @endif>
                    {{ $row[$item->nameField] }}
                </option>
            @endforeach
        </select>
    </div>
</div>