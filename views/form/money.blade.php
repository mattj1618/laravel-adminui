
<div class="form-group">
    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>
    <div class="input-group col-sm-10">
        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
        <input class="form-control" type="number" name="{{ $item->id }}" value="{{ $item->defaultValue }}" @if($item->disabled) readonly @endif/>
        <span class="input-group-addon">.00</span>
    </div>
</div>