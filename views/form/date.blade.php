<script type="text/javascript">
    $(function() {
        $('#{{ $item->id }}').datepicker({
            format: 'dd/mm/yyyy'
        })
    });
</script>
<div class="form-group">
    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>
    <div class="col-sm-10">
        <input class="form-control" type="text" name="{{ $item->id }}" id="{{ $item->id }}" value="{{ $item->defaultValue }}"/>
    </div>
</div>