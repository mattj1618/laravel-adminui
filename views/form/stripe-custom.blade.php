
<!-- InputMask -->
{{--<script src="https://cdn.communitytogo.com.au/input-mask/jquery.inputmask.js" type="text/javascript"></script>--}}
{{--<script src="https://cdn.communitytogo.com.au/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>--}}
{{--<script src="https://cdn.communitytogo.com.au/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>--}}

<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
{{--<script type="text/javascript">--}}
    {{--$(function () {--}}
        {{--//Datemask dd/mm/yyyy--}}
        {{--$("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});--}}
        {{--//Datemask2 mm/dd/yyyy--}}
        {{--$("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});--}}
        {{--//Money Euro--}}
        {{--$("[data-mask]").inputmask();--}}
    {{--});--}}
{{--</script>--}}
{{--<button class="btn btn-{{Config::get('admin-ui.default-btn-type')}}" id="purchaseButton">Purchase</button><br/><br/>--}}
<input type="hidden" name="{{$item->id}}" id="{{$item->id}}" />

<span class="payment-errors" style="color:red"></span>

<div class="form-row form-group">
    <label>
        <span>Card Number</span>
    </label>
    <input type="text" size="20" maxlength="20" style="width:auto; min-width:200px"  data-stripe="number" class="form-control" placeholder="Credit Card Number"/>
</div>

<div class="form-row form-group">
        <label>
            <span>CVC</span>
        </label>
        <input type="text" size="4" maxlength="4" style="width:auto; min-width:80px" data-stripe="cvc" class="form-control" placeholder="(e.g. 123)"/>

</div>
<div class="form-row form-group">
        <label>
            <span>Expiration</span>
        </label><br/>
        <input type="text" size="2" maxlength="2" style="width:auto; min-width:50px;display: inline;" class="form-control" data-stripe="exp-month" placeholder="MM"/>
        {{--<span class="col-xs-1" style="height:34px;"> / </span>--}}
        <input type="text" size="4" maxlength="4" style="width:auto; min-width:50px;display: inline;" class="form-control" data-stripe="exp-year" placeholder="YYYY"/>

</div>


{{--<div style=" height:20px;width:100%; padding-bottom: 20px;"></div>--}}
{{--<div class="form-row">--}}
    {{--<label>Expiration</label>--}}
    {{--<div class="expiry-wrapper">--}}
        {{--<select class="card-expiry-month stripe-sensitive required" data-stripe="exp-month">--}}
        {{--</select>--}}
        {{--<script type="text/javascript">--}}
            {{--var select = $(".card-expiry-month"),--}}
                    {{--month = new Date().getMonth() + 1;--}}
            {{--for (var i = 1; i <= 12; i++) {--}}
                {{--select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))--}}
            {{--}--}}
        {{--</script>--}}
        {{--<span> / </span>--}}
        {{--<select class="card-expiry-year stripe-sensitive required" data-stripe="exp-year"></select>--}}
        {{--<script type="text/javascript">--}}
            {{--var select = $(".card-expiry-year"),--}}
                    {{--year = new Date().getFullYear();--}}

            {{--for (var i = 0; i < 12; i++) {--}}
                {{--select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))--}}
            {{--}--}}
        {{--</script>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<br/><br/>--}}
<script type="text/javascript">
    // This identifies your website in the createToken call below
    Stripe.setPublishableKey('{{Config::get('admin-ui.stripe-pk')}}');

    var stripeResponseHandler = function(status, response) {
        var $form = $('#Buy-Tickets-form');

        if (response.error) {
            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('input[type="submit"]').prop('disabled', false);
        } else {
            // token contains id, last4, and card type
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
//            $form.append($('<input type="hidden" name="stripeToken" />').val(token));

            var id = "{{$item->id}}";
            $('#'+id).attr('value',token);

            // and re-submit
            $form.get(0).submit();
        }
    };

    jQuery(function($) {
        $('#Buy-Tickets-form').submit(function(e) {
            var $form = $(this);

            // Disable the submit button to prevent repeated clicks
            $form.find('input[type="submit"]').prop('disabled', true);

            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from submitting with the default action
            return false;
        });
    });
</script>