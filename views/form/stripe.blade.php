<script src="https://checkout.stripe.com/checkout.js"></script>

<button class="btn btn-{{Config::get('admin-ui.default-btn-type')}}" id="purchaseButton">Purchase</button><br/><br/>
<input type="hidden" name="{{$item->id}}" id="{{$item->id}}" />
<script>
    var formId = '{{$item->formId}}';

    var handler = StripeCheckout.configure({
        key: '{{Config::get('admin-ui.stripe-pk')}}',
//        image: '/img/documentation/checkout/marketplace.png',
        token: function(token) {
            var id = "{{$item->id}}";
            $('#'+id).attr('value',token.id);
            $('#'+formId).submit();

            // Use the token to create the charge with a server-side script.
            // You can access the token ID with `token.id`
        }
    });
    var amount = '{{$item->amount}}';

    $('#purchaseButton').on('click', function(e) {
        // Open Checkout with further options
        handler.open({
            name: '{{$item->name}}',
            description: '{{$item->description}}',
            currency: "aud",
            amount: amount
        });
        e.preventDefault();
    });

    // Close Checkout on page navigation
    $(window).on('popstate', function() {
        handler.close();
    });
</script>