
<div class="form-group">
    <label class="control-label col-sm-2">{{ $item->label }}</label>

    <div class="col-sm-10">
        <p>{{ $item->defaultValue }}</p>
    </div>
</div>