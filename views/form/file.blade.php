
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>

        <div class="col-sm-10">
            <a href="/{{$item->filename}}" target="_blank">Download</a>
            <span>{{$item->filename}}</span>
            <input  type="file" name="{{ $item->id }}"/>
        </div>
    </div>