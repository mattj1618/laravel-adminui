
<div class="form-group">
    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>

    <div class="col-sm-10">
        <input class="form-control" type="password" name="{{ $item->id }}" placeholder="{{ $item->placeHolder }}"/>
    </div>
</div>