
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{ $item->id }}" class="col-sm-2 control-label">{{ $item->label }}</label>
        <div class="col-sm-10">
            {{--<input class="form-control" type="text" name="{{ $item->id }}" value="{{ $item->defaultValue }}"/>--}}
            <button data-inputname="{{$item->id}}" id="{{$item->id}}" class="btn btn-default">Select File</button>
            <input type="hidden" value="{{$item->defaultValue}}" name="{{$item->id}}">
        </div>
    </div>

    <script type="text/javascript">

        // function to update the file selected by elfinder
        function processSelectedFile(filePath, requestingField) {
            $('[name="' + requestingField+'"]').val(filePath);
            $('#' + requestingField).text("Selected File: "+filePath);

        }

        $(function(){
            $(document).on('click','#{{$item->id}}',function (event) {
                event.preventDefault();
                var updateID = $(this).attr('data-inputname'); // Btn id clicked
                var elfinderUrl = '/elfinder/popup/';

                // trigger the reveal modal with elfinder inside
                var triggerUrl = elfinderUrl + updateID;
                $.colorbox({
                    href: triggerUrl,
                    fastIframe: true,
                    iframe: true,
                    width: '70%',
                    height: '500px'
                });

            });
        });

    </script>