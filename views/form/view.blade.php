@if(isset($item->lgCols))
    <div class="col-lg-{{$item->lgCols}}  form-horizontal">
@endif
        <label class="control-label col-sm-2" for="{{$item->id}}">{{ $item->label }}</label>
        <div class="col-sm-10" style="padding-top:7px;">
            @if($item->defaultValue!=='')
                {!! $item->defaultValue !!}
            @else
                None
            @endif
        </div>
@if(isset($item->lgCols))
    </div>
@endif