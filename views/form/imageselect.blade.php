{{--<div class="form-group">--}}
    {{--<label for="{{ $item->id }}">{{ $item->label }}</label>--}}
    {{--<select name="{{ $item->id }}" class="form-control select2" {{$item->disabled===true?'disabled':''}}>--}}

        {{--@if($item->nullable===true)--}}
            {{--<option value="none">--}}
                {{--None--}}
            {{--</option>--}}
        {{--@endif--}}

        {{--@foreach($item->rows as $row)--}}
            {{--<option value="{{ $row[$item->idField] }}"--}}
                    {{--@if( $item->isDefault( $row[$item->idField] ))--}}
                        {{--selected--}}
                    {{--@endif>--}}
                {{--<div>--}}
                    {{--<img src="/{{ $row[$item->valueField] }}" style="max-width:30px;max-height:30px;">--}}
                    {{--{{ $row[$item->nameField] }}--}}
                {{--</div>--}}
            {{--</option>--}}
        {{--@endforeach--}}
    {{--</select>--}}
{{--</div>--}}
<div class="form-group">
    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>
    <div class=" col-sm-10">
        <div id="imgselect-{{ $item->id }}" class="btn-group" style="margin:10px;">    <!-- CURRENCY, BOOTSTRAP DROPDOWN -->
            <!--<a class="btn btn-primary" href="javascript:void(0);">Currency</a>-->

            <input type="hidden" name="{{ $item->id }}" value="{{$item->defaultValue}}">

            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
                @if(!isset($item->defaultValue))
                    Select Image
                @else
                    Image Selected - {{$item->defaultValue}}
                @endif
                <span class="caret"></span>
            </a>


            <ul class="dropdown-menu scrollable-menu" style="height: auto;max-height: 600px;overflow-x: hidden;border:1px #777 solid">
                @if($item->nullable)
                    <li><a href="javascript:void(0);" value="">
                            <span   >None</span></a>
                    </li>
                @endif
                @foreach($item->rows as $row)
                    <li><a href="javascript:void(0);" value="{{ $row[$item->idField] }}">
                            <img src="/{{ $row[$item->valueField] }}" style="max-width:200px;max-height:100px;" /> <span style="display:none">{{ $row[$item->nameField] }}</span></a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>


<script type="text/javascript">
/* BOOTSTRAP DROPDOWN MENU - Update selected item text and image */
//var id = "";
$("#imgselect-{{ $item->id }} li a").click(function () {

    var selText = $(this).text();
    var imgSource = $(this).find('img').attr('src');
    var img = '<img src="' + imgSource + '"/>';

    $('input[name="{{ $item->id }}"]').val($(this).attr('value'));

    $(this).parents('.btn-group').find('.dropdown-toggle').html( ' ' + selText + ' <span class="caret"></span>');
});
</script>