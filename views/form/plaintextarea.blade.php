
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{$item->id}}">{{$item->label}}</label>

        <div class="col-sm-10">
            <textarea rows="10" class="form-control" type="text" name="{{$item->id}}">{{$item->defaultValue}}</textarea>
        </div>
    </div>

{{--<script type="text/javascript">--}}
    {{--$('textarea[name="{{$item->id}}"]').tinymce({--}}
        {{--plugins: [--}}
            {{--"advlist autolink lists link image charmap print preview anchor",--}}
            {{--"searchreplace visualblocks code fullscreen",--}}
            {{--"insertdatetime media table contextmenu paste"--}}
        {{--],--}}
        {{--toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"--}}
    {{--});--}}
{{--</script>--}}