@if(isset($progress))
    <ol class="progtrckr" data-progtrckr-steps="{{count($progress)}}">@foreach($progress as $p)<a href="{{$p["link"]}}"><li  class="progtrckr-{{$p["progress"]}}">{{$p["name"]}}</li></a>@endforeach
    </ol>
@endif