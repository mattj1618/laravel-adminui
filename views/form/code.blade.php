
<script src="https://cdn.communitytogo.com.au/ace/ace.js" type="text/javascript" charset="utf-8"></script>
<style type="text/css" media="screen">
    .notFullScreen {
        position: relative;
        width:100%;
        height:500px;
    }

</style>
<div class="form-group">
    <label for="{{ $item->id }}">{{ $item->label }}</label>

    <textarea name="{{$item->id}}">{{ $item->defaultValue }}</textarea>
    <div id="{{ $item->id }}" class="notFullScreen"></div>

    <script>

//        $(function(){
//            var dom = require("ace/lib/dom");
//            var commands = require("ace/commands/default_commands").commands;
// add command for all new editors
//            commands.push({
//                name: "Toggle Fullscreen",
//                bindKey: "F11",
//                exec: function(editor) {
//                    dom.toggleCssClass(document.body, "fullScreen");
//                    dom.toggleCssClass(editor.container, "fullScreen-editor");
//                    editor.resize()
//                }
//            });

            var id = "{{ $item->id }}";
            var editor = ace.edit(id);
            var textarea = $('textarea[name="'+id+'"]').hide();
            editor.getSession().setValue(textarea.val());
            editor.getSession().on('change', function(){
                var id = "{{ $item->id }}";
//                alert(id);
                var textarea = $('textarea[name="'+id+'"]');
                console.log(textarea);
                textarea.text(editor.getSession().getValue());
            });
            editor.setTheme("ace/theme/monokai");
            editor.getSession().setMode("ace/mode/html");

//            var editor = ace.edit(id);
//            editor.commands.addCommand({
//                name: "Toggle Fullscreen",
//                bindKey: "F11",
//                exec: function(editor) {
////                    alert('');
//                    $('#'+id).toggleClass('fullScreen');
//                    $('#'+id).toggleClass('notFullScreen');
//                    editor.resize();
//                }
//            });
//        });
    </script>

</div>