<h1>{{ $item->label }}

    @if(isset($item->views))
        @foreach($item->views as $view)
            {!! $view->render() !!}
        @endforeach
    @endif

</h1>