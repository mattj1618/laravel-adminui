@if(isset($item->lgCols))
    <div class="col-lg-{{$item->lgCols}} form-horizontal">
@endif
        <div class="form-group">
            <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>
            <p class="col-sm-10" style="padding-top:7px;">
                <a href="/{{$item->filename}}" target="_blank">{{$item->filename}}</a>
            </p>
            {{--<input  type="file" name="{{ $item->id }}"/>--}}
        </div>
@if(isset($item->lgCols))
    </div>
@endif