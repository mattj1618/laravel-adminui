@if(isset($item->lgCols))
    <div class="col-lg-{{$item->lgCols}} form-horizontal">
@endif
        <label class="control-label col-sm-2" for="{{$item->id}}">{{ $item->label }}</label>
        <p class="col-sm-10" style="padding-top:7px;">
            @if(isset($item->defaultValue) && strlen($item->defaultValue)>0 && $item->defaultValue!=='None')
                <img src="/{{ $item->row[$item->valueField] }}" style="max-width:200px;max-height:100px;">
                {!! $item->defaultValue !!}
            @else
                None
            @endif
        </p>
@if(isset($item->lgCols))
    </div>
@endif