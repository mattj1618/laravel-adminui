
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{ $item->id }}" class="col-sm-2 control-label">{{ $item->label }}</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" name="{{ $item->id }}" value="{{ $item->defaultValue }}"/>
        </div>
    </div>