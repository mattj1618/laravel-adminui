
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{$item->id}}">{{$item->label}}</label>

        <div class="col-sm-10">
            <textarea rows="10" class="form-control" type="text" name="{{$item->id}}">{{$item->defaultValue}}</textarea>
        </div>
    </div>
<script type="text/javascript">
    $(function(){

        function elFinderBrowser (field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: '<?= route('elfinder.tinymce4') ?>',// use an absolute path!
                title: 'elFinder 2.0',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    srcControl = win.document.getElementById(field_name);
                    srcControl.value = url;
                    tinymce.dom.DOMUtils.DOM.fire(srcControl, 'change', {control: srcControl});
                }
            });
            return false;
        }

        $('textarea[name="{{$item->id}}"]').tinymce({
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor colorpicker textpattern imagetools"
            ],
            relative_urls: false,
            file_browser_callback : elFinderBrowser,
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor ",
            {{--@if($item->images)--}}
                {{--image_list: [--}}
                    {{--@foreach($item->images as $img)--}}
                        {{--{title: '{{$img->filename}}', value: '{{$img->value}}'}--}}
                {{--@endforeach--}}
            {{--]--}}
            {{--@endif--}}
        });

    });

</script>