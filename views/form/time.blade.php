
<!-- time Picker -->
<div class="bootstrap-timepicker">
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>
        <div class="input-group col-sm-10">
            <input class="form-control timepicker" type="text" name="{{ $item->id }}" id="{{ $item->id }}" value="{{ $item->defaultValue }}"/>
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
        </div><!-- /.input group -->
    </div>
</div>
    <script type="text/javascript">
        $(function () {
            //Timepicker
            $("#{{ $item->id }}").timepicker({
                showInputs: false
            });
        });
    </script>