
    <div class="form-group">
        <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>

        <div class="col-sm-10">
            <span>{{$item->filename}}</span>
            @if(isset($item->defaultValue) && strlen($item->defaultValue)>0)
                <img src="/{{$item->defaultValue}}" style="max-width:100px;max-height:100px;"/>
            @endif
            <input  type="file" name="{{ $item->id }}"/>
        </div>
    </div>