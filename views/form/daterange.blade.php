
<div class="form-group">

    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>
    <div class="input-group col-sm-10">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input class="form-control pull-right" type="text" name="{{ $item->id }}" id="{{ $item->id }}" value="{{$item->startDate}} - {{$item->endDate}}"  {{$item->disabled===true?'disabled':''}}/>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#{{ $item->id }}').daterangepicker(
                {
                    startDate: '{{$item->startDate}}',
                    endDate: '{{$item->endDate}}',
                    timePicker: false,
                    format: 'MMMM D, YYYY'
                });

        {{--$('#{{ $item->id }}').data('daterangepicker').setStartDate('{{$item->startDate}}');--}}
        {{--$('#{{ $item->id }}').data('daterangepicker').setEndDate('{{$item->endDate}}');--}}
    });
</script>