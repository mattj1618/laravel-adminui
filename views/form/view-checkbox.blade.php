@if(isset($item->lgCols))
    <div class="col-lg-{{$item->lgCols}} form-horizontal">
@endif
        <label class="control-label col-sm-2" for="{{$item->id}}">{{ $item->label }}</label>
        <p class="col-sm-10" style="padding-top:7px;">
            @if(isset($item->defaultValue))
                {{ $item->defaultValue===1||$item->defaultValue==='1' ? 'Yes':'No' }}
            @else
                None
            @endif
        </p>
@if(isset($item->lgCols))
    </div>
@endif