<div class="form-group">

    <label class="control-label col-sm-2" for="{{$item->id}}">
        {{ $item->label  }}
    </label>

    <div class="col-sm-10">

        @foreach($item->rows as $row)
            <div class="radio radio-{{ Config::get('admin-ui.default-btn-type') }}">
                <input type="radio" name="{{ $item->id }}" id="{{ $row[$item->idField] }}" value="{{ $row[$item->idField] }}">
                <label for="{{ $row[$item->idField] }}">
                    {{ $row[$item->nameField] }}
                </label>
            </div>
        @endforeach
    </div>

</div>