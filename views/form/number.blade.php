
<div class="form-group">
    <label class="control-label col-sm-2" for="{{ $item->id }}">{{ $item->label }}</label>

    <div class="col-sm-10">
        <input class="form-control" type="number" step="any" name="{{ $item->id }}" value="{{ $item->defaultValue }}" @if($item->disabled) readonly @endif/>
    </div>
</div>