
{{--<div class="form-group">--}}
{{--<input id="{{ $list["id"] }}-input" class="form-control" type="search" placeholder="Search..." />--}}
{{--</div>--}}
@if(isset($table["print"]))
    <a href="{{$table["print"]}}" class="btn btn-primary pull-right" target="_blank">Print</a>
@endif
@if(isset($table["export"]))
    <a href="{{$table["export"]}}" class="btn btn-primary pull-right" style="margin-right:10px;" target="_blank">Export</a>
@endif
<div style='width:100%; overflow-x: auto'>
    <table class="table table-striped table-bordered" id="{{ $table["id"] }}-table">
        <thead>
        <tr>
            @foreach($table["rows"][0]["cols"] as $col)
                <th>
                    @if(is_array($col["value"]))
                        @foreach($col["value"] as $v)
                            {{$v}}<br/>
                        @endforeach
                    @else
                        {{$col["value"]}}
                    @endif
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach(array_slice($table["rows"],1) as $row)
            <tr>
                @foreach($row["cols"] as $col)
                    <td>
                        @if(isset($col["isView"]))
                            {!!$col["view"]->render()!!}
                        @elseif(isset($col["isViews"]))
                            @foreach($col["views"] as $view)
                                {!!$view->render()!!}
                            @endforeach
                        @elseif(is_array($col["value"]))
                            @foreach($col["value"] as $v)
                                {!!$v!!}
                            @endforeach
                        @else
                            {!!$col["value"]!!}
                        @endif
                    </td>
                @endforeach
            </tr>

        @endforeach
        </tbody>
    </table>
</div>

{{--<script>--}}
{{--$(document).ready(function () {--}}

{{--(function ($) {--}}

{{--var id = '{{ $list["id"] }}';--}}

{{--$('#'+id+'-input').keyup(function () {--}}

{{--var rex = new RegExp($(this).val(), 'i');--}}
{{--$('#'+id+'-list li').hide();--}}
{{--$('#'+id+'-list li').filter(function () {--}}
{{--return rex.test($(this).text());--}}
{{--}).show();--}}

{{--})--}}

{{--}(jQuery));--}}

{{--});--}}
{{--</script>--}}


