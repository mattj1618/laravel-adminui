@extends('admin-ui::layout.plain-page')

@section('content')
    <div class="error-page">
        <h2 class="headline text-yellow"> 500</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Internal Server Error.</h3>
            <p>
                Sorry, something went wrong. Please contact support at <a href="mailto:info@communitytogo.com">info@communitytogo.com</a> or on +61 8 6102 5117
            </p>

        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@stop