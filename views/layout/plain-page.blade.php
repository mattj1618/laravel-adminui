<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>{{ Config::get('admin-ui.title') }}</title>
    @include("admin-ui::common.scripts")
</head>

<body class="skin-{{ Config::get('admin-ui.colour') }} layout-boxed  sidebar-collapse">
<!-- header logo: style can be found in header.less -->

<div class="wrapper">

    @include("admin-ui::common.header")

    <script type="text/javascript">
        $(document).ready(function() {
            $('#menu-button').hide();
//            $('#top-navbar').css({ 'opacity' : 0.0 });
        });
    </script>

    @include("admin-ui::common.menu")

    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        {{--<section class="content-header">--}}
        {{--<h1>--}}
        {{--{{$header}}--}}
        {{--<small></small>--}}

        {{--</h1>--}}

        {{--</section>--}}

        <!-- Main content -->
        <section class="content">


            @include("admin-ui::common.alerts")

            @yield('content')





        </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->
</div>
</body>
</html>
