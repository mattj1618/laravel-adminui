<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>{{ Config::get('admin-ui.title') }}</title>
    @include("admin-ui::common.scripts")
</head>

<body class="login-page" style="background-color: white">
<div class="login-box">
    <div class="login-logo">
        <a href="{{isset($titleUrl)?$titleUrl:'/'}}"><b>@yield('header')</b></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body" style="border: 1px solid #ccc;">
        @yield('content')
    </div><!-- /.login-box-body -->
    @if(Config::get('admin-ui.contact')!==null)
        <p>Contact Support: {!! isset($contact) ? $contact : Config::get('admin-ui.contact') !!}</p>
    @endif
</div><!-- /.login-box -->
</body>
</html>
