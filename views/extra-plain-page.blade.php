@extends('admin-ui::layout.extra-plain-page')

@section('content')
    @if(isset($views))
        @foreach($views as $view)
            {!! $view->render() !!}
        @endforeach
    @endif
@stop