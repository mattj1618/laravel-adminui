@if(isset($lg))
    <div class="col-lg-{{$lg}} col-lg-push-{{$push}}">
        @endif
        <div class='box {{isset($boxClass)?$boxClass:'box-default'}} {{$collapse?'collapsed-box':''}}'>
            <div class='box-header with-border'>
                <h3 class='box-title'>@if(isset($icon))<i class="fa {{$icon}}"></i>@endif {{ $header }}</h3>

                @if(isset($buttons))
                    @foreach($buttons as $button)
                        <a href="{{$button["href"]}}" class="btn btn-default btn-sm" style="min-width:80px; margin:0 5px;">{{$button["label"]}}</a>
                    @endforeach
                @endif

                <div class="box-tools pull-right">

                    {{--<button class="btn btn-box-tool" data-widget="collapse"></button>--}}


                    @if($collapse===true)
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    @endif
                </div>
            </div>
            <div class='box-body'>
                @if(isset($views))
                    @foreach($views as $view)
                        {!! $view->render() !!}
                    @endforeach
                @endif
            </div>
        </div>
        @if(isset($lg))
    </div>
@endif