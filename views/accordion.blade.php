<div class="box-group" id="{{$id}}">
    @foreach($panels as $panel)
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">@if(isset($panel["icon"]))<i class="fa {{$panel["icon"]}}"></i>@endif
                    <a data-toggle="collapse" data-parent="#{{$id}}" href="#{{$panel["id"]}}" class="collapsed">
                        {{$panel["header"]}}
                    </a>

                </h4>

                @if(isset($panel["buttons"]))
                    @foreach($panel["buttons"] as $button)
                        {{--<div class="pull-right" style="float:right">--}}
                            <a href="{{$button->defaultValue}}" class="btn btn-default pull-right btn-sm" tyle="float:right;" style="min-width:80px; margin:0 5px;">{{$button->label}}</a>
                        {{--</div>--}}
                    @endforeach
                @endif
            </div>
            <div id="{{$panel["id"]}}" class="panel-collapse collapse">
                <div class="box-body">
                    @if(is_array($panel["body"]))
                        @foreach($panel["body"] as $i)
                            {!!$i->render()!!}
                        @endforeach
                    @else
                        {!!$panel["body"]->render()!!}
                    @endif
                </div>
            </div>
        </div>
    @endforeach
</div>