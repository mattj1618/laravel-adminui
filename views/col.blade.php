
<div class="{{$classes}}" style="padding:20px;" >
        @if(isset($views))
            @foreach($views as $view)
                {!! $view->render() !!}
            @endforeach
        @endif
</div>
