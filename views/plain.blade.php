
    @if(isset($views))
        @foreach($views as $view)
            {!! $view->render() !!}
        @endforeach
    @endif