<style>
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent!important;
    }
</style>
<form action="{{$actionUrl}}" method="POST">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

    <div class="example-modal">
        <div class="modal modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{$header}}</h4>
                    </div>
                    <div class="modal-body">
                        <p>{{$body}}</p>
                    </div>
                    <div class="modal-footer">
                        @if(isset($buttons))
                            @foreach($buttons as $button)
                                @if(isset($button["submit"]) && $button["submit"]===true)
                                    <input type="submit" class="btn btn-{{$button["type"]}}" value="{{$button["label"]}}" />
                                @else
                                    <a href="{{$button["href"]}}" class="btn btn-{{$button["type"]}}" style="min-width:80px; margin:0 5px;">{{$button["label"]}}</a>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div><!-- /.example-modal -->
</form>


