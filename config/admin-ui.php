<?php
return [
    'name1' => 'Simple',
    'name2' => 'Swim',
    'title' => 'Simple Swim',
    'logo' => null,
    'colour' => 'blue',
    'default-btn-type' => 'default',
    'stripe' => false,
    'stripe-pk' => '',
    'stripe-sk' => '',
    'file-upload-dir' => 'upload',
    'contact'=>"<a href='info@communitytogo.com'>info@communitytogo.com</a>",
    'menu-collapsed' => false
];