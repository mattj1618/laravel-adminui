<?php namespace MJ1618\AdminUI\Utils;

use Illuminate\Support\Facades\View;
use Log;
class ViewUtils {
    public static function page($views){
        return View::make("admin-ui::page")->with('views',$views);
    }
    public static function plainPage($views){
        return View::make("admin-ui::plain-page")->with('views',$views);
    }
    public static function extraPlainPage($views){
        return View::make("admin-ui::extra-plain-page")->with('views',$views);
    }
    public static function printPage($views){
        return View::make("admin-ui::print")->with('views',$views);
    }
    public static function box($header,$views,$lg=null,$push=null,$collapse=false,$buttons=[],$icon=null,$boxClass=null)
    {
        return View::make("admin-ui::box")
            ->with('header',$header)
            ->with('views',$views)
            ->with('lg',$lg)
            ->with('push',$push)
            ->with('collapse',$collapse)
            ->with('buttons',$buttons)
            ->with('icon',$icon)
            ->with('boxClass',$boxClass);
    }
    public static function accordion($id,$header,$panels)
    {
        return View::make("admin-ui::accordion")
            ->with('header',$header)
            ->with('panels',$panels)
            ->with('id',$id);
    }
    public static function boxCollapse($header,$views,$lg=null,$push=null)
    {
        return self::box($header,$views,$lg,$push,true);
    }

    public static function modal($header,$body,$buttons,$actionUrl)
    {
        return View::make("admin-ui::modal")
            ->with('header',$header)
            ->with('body',$body)
            ->with('buttons',$buttons)
            ->with('actionUrl',$actionUrl);
    }
    public static function loginForm($header,$views)
    {
        return View::make("admin-ui::login-form")->with('header',$header)->with('views',$views);
    }

    public static function box6($header,$views)
    {
        return self::box($header,$views,6,3);
    }
    public static function box8($header,$views)
    {
        return self::box($header,$views,8,2);
    }
    public static function listView($list)
    {
        return View::make("admin-ui::list")->with('list',$list);
    }
    public static function plain($views)
    {
        return View::make("admin-ui::plain")->with('views',$views);
    }
    public static function crumbs($name, $parentId=null, $id=null){
        return View::make('admin-ui::layout/crumbs')->with('content',\Breadcrumbs::render($name, $parentId,$id));
    }
    public static function col6($views)
    {
        return self::col($views,'col-lg-6');
    }
    public static function col12($views)
    {
        return self::col($views,'col-lg-12');
    }
    public static function col($views,$classes)
    {
        return View::make("admin-ui::col")->with('views',$views)->with('classes',$classes);
    }
    public static function row($views)
    {
        return View::make("admin-ui::row")->with('views',$views);
    }
    public static function blank()
    {
        return View::make("admin-ui::blank");
    }

    public static function progress($progress)
    {
        Log::error(json_encode($progress));
        return View::make("admin-ui::form/progress")->with('progress',$progress);
    }
}