<?php namespace MJ1618\AdminUI\Utils;

use Illuminate\Support\Facades\View;
use Log;
class ViewWrapper {

    var $fn;

    function __construct($fn){
        $this->fn = $fn;
    }

    function render(){
        return $this->fn->__invoke();
    }

}