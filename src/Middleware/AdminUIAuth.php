<?php namespace MJ1618\AdminUI\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class AdminUIAuth {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

        if(!Auth::check()){
            Session::put('return_url',Request::url());
            \Debugbar::info('url:'.Session::get('return_url'));
            return redirect()->to('/login');
        }

		return $next($request);
	}

}
