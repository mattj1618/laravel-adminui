<?php namespace MJ1618\AdminUI;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
class AdminUIServiceProvider extends BaseServiceProvider {
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    // Can't enable this because there appears to be a bug in Laravel where a
    // non-deferred service provider can't use a deferred one because the boot
    // method is not called - see DependantServiceProviderTest.
    // protected $defer = true;
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['admin-ui'];
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {

        $viewPath = __DIR__ . '/../views';
        $this->loadViewsFrom($viewPath, 'admin-ui');
        $this->publishes([
            $viewPath => base_path('resources/views/vendor/admin-ui')
        ],'admin-ui-views');

//        $publicPath = __DIR__.'/../public';
//        $this->publishes([
//            $publicPath => public_path('')
//        ],'admin-ui-public');

        $configFile = __DIR__ . '/../config/admin-ui.php';
        $this->mergeConfigFrom($configFile, 'admin-ui');
        $this->publishes([
            $configFile => config_path('admin-ui.php')
        ],'admin-ui-config');

    }
}