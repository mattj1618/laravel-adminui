<?php namespace MJ1618\AdminUI\Controller;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Input;
use MJ1618\AdminUI\Form\DateRange;
use MJ1618\AdminUI\Form\DateTimeRange;
use MJ1618\AdminUI\Form\DropDown;
use MJ1618\AdminUI\Form\MetaItem;
use MJ1618\AdminUI\Form\TextAreaBox;
use MJ1618\AdminUI\Utils\ViewUtils;
use Illuminate\Support\Facades\Log;
use Breadcrumbs;

class FormController extends Controller {

    var $template = 'admin-ui::form/edit';
    var $controllerClass;
    var $header;
    var $parentBreadcrumb;
    var $baseRoute;
    var $extraPlainPage = false;
    var $plainPage = false;
    var $loginForm = false;
    var $infoMessages=[];
    var $errorMessages=[];
    var $successMessages=[];
    var $suffix = '/form';
    var $btnClasses='col-lg-2 col-md-3';


    function getUrl(){ return route($this->getHeader(), Route::current()->parameters()); }


    function beforeShow(){
        return null;
    }

    function beforePost(){
        return null;
    }

    //Create
    function show(){

        $b = $this->beforeShow();
        if($b!==null)return $b;

        $views=[];
        return $this->showView($views);
    }

    function showView($views){

        if($this->loginForm){
            return $this->showLoginForm();
        }

        $allViews = array_merge([
            $this->getCrumbs(),
            $this->showViewBox()

        ],$views);

        if($this->extraPlainPage===true){
            return ViewUtils::extraPlainPage($allViews);
        } else if($this->plainPage===true){
            return ViewUtils::plainPage($allViews);
        } else {
            return ViewUtils::page($allViews);
        }
    }

    function showLoginForm(){

//        $model = [
//            'actionUrl'=>$this->getUrl().$this->suffix,
//            'form'=>$this->definition()
//        ];

        if(Session::has('error'))
            $this->errorMessages = array_merge($this->errorMessages, [Session::get('error')]);
        if(Session::has('success'))
            $this->successMessages = array_merge($this->successMessages, [Session::get('success')]);

        return ViewUtils::loginForm(
            $this->getHeader(),
            [
                View::make($this->template)
                    ->with('form',$this->definition())
                    ->with('actionUrl',$this->getUrl().$this->suffix)
            ]);
    }

    function showViewBox(){

//        $model = [
//            'actionUrl'=>$this->getUrl().$this->suffix,
//            'form'=>$this->definition()
//        ];

        if(Session::has('error'))
            $this->errorMessages = array_merge($this->errorMessages, [Session::get('error')]);
        if(Session::has('success'))
            $this->successMessages = array_merge($this->successMessages, [Session::get('success')]);

        return ViewUtils::box(
            $this->getHeader(),
            [
                View::make($this->template)
                    ->with('form',$this->definition())
                    ->with('actionUrl',$this->getUrl().$this->suffix)
            ]);
    }

    function basePost(){
        $resp = $this->beforePost();
        if(isset($resp)){
            return $resp;
        }
        return $this->post();
    }

    function post(){
        return Redirect::to($this->getUrl());
    }

    function routes(){
        $this->showRoute();
    }

    function breadcrumbs(){
        $this->showBreadcrumb();
    }

    function showRoute(){
        Route::get($this->baseRoute.$this->suffix, ['as' => $this->getHeader(), 'uses'=>"$this->controllerClass@show"]);
        Route::post($this->baseRoute.$this->suffix, "$this->controllerClass@basePost");
    }

    function showBreadcrumb(){
        Breadcrumbs::register($this->getHeader(), function($breadcrumbs, $parentId) {
            $breadcrumbs->push($this->getHeader(), route($this->getHeader()));
        });
    }


    function processInputs($row){
        $form = $this->definition();
        foreach($form['Inputs'] as $input){
            $this->insertItem($row,$input);
        }
    }
    function insertItem($row, $input){
        if($input->doInsert) {
            if (Input::has($input->id)) {
                $input->insert($row, Input::get($input->id));
            } else {
                $input->insert($row, $input->defaultValue);
            }
        }
    }

    function definition($def=[]){
        return array_merge($def,[
            'Info Messages' => $this->infoMessages,
            'Error Messages' => $this->errorMessages,
            'Success Messages' => $this->successMessages,
            'Button Classes' => $this->btnClasses,
        ]);
        
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return mixed
     */
    public function getParentBreadcrumb()
    {
        return $this->parentBreadcrumb;
    }

    function getCrumbs()
    {
        return ViewUtils::crumbs($this->getHeader());
    }


}