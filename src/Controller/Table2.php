<?php namespace MJ1618\AdminUI\Controller;


use Input;
use MJ1618\AdminUI\Utils\ViewUtils;
use Breadcrumbs;
use Route;
use Log;
use Request;
use URL;
class Table2 extends BaseTable2 {

    function getAjaxBaseRoute(){                return $this->ajaxBaseRoute; }
    function getAjaxRoute(){                    return $this->getAjaxBaseRoute(); }
    function getCreateRoute(){                  return "$this->baseRoute/create"; }
    function getEditRoute(){                    return "$this->baseRoute/{id$this->level}/edit"; }
    function getAjaxDeleteRoute(){              return $this->getAjaxBaseRoute()."/{id$this->level}/delete"; }

    function getViewRoute(){                    return $this->getBaseRoute()."/{id$this->level}/view";}

    function getBaseUrl(){                      return route($this->getHeaderSingular()." Table",$this->currentParamsLess()); }
    function getCreateUrl(){                    return route($this->getHeaderSingular()." Create",$this->currentParamsLess()); }
    function getAjaxBaseUrl(){                  return route($this->getHeaderSingular()." Ajax",$this->currentParamsLess()); }

    function getEditUrl(){                      return route($this->getHeaderSingular()." Edit",$this->currentParamsMore());    }
    function getAjaxUrl(){                      return $this->getAjaxBaseUrl(); }
    function getCreateReturnUrl($id){
        if($this->parentHeader===null)
            return route($this->getHeaderSingular()." Table",$this->currentParamsLess())."/$id/view";
        else {
            return route($this->parentHeader." View",$this->currentParamsLess());
        }
    }
    function getEditReturnUrl(){
        if($this->parentHeader===null)
            return route($this->getHeaderSingular()." View",$this->currentParamsMore());
        else {
            return route($this->parentHeader." View",$this->currentParamsLess());
        }
    }

    function getEditPartialRoute(){             return $this->getBaseUrl()."/{id}/edit"; }
    function getViewPartialRoute(){             return $this->getBaseUrl()."/{id}/view"; }
    function getAjaxDeletePartialRoute(){              return $this->getAjaxBaseUrl()."/{id}/delete"; }

    function buttons(){
        return [
//            "edit" => [
//                'id'=>$this->getHeaderSingular()."-edit",
//                'text'=>'Edit',
//                'requiresSelect'=>'true',
//                'url'=>$this->getEditPartialRoute()
//            ],
            "view" => [
                'id'=>$this->getHeaderSingular()."-view",
                'text'=>'View/Edit',
                'requiresSelect'=>'true',
                'url'=>$this->getViewPartialRoute()
            ],
            "create" => [
                'id'=>$this->getHeaderSingular()."-create",
                'text'=>'Create',
                'requiresSelect'=>'false',
                'url'=>$this->getCreateUrl(),
                'float'=>'left'
            ]
        ];
    }

    function __construct(){
    }


    function tableDefinition(){
        return [
            'header' => $this->getHeaderSingular(),
            'tableName'=> $this->tableName,
            'attributes' => $this->getAttributes(),
            'buttons'=>$this->buttons(),
            'deleteButton'=>$this->useDeleteButton,
            'ajaxDeletePartialUrl'=>$this->getAjaxDeletePartialRoute(),
            'ajaxUrl'=>$this->getAjaxUrl(),
            'rows' => $this->useTableAjax ? '' : $this->dataAll(),
            'customDataEdit'=>$this->getCustomDataEdit(),
            'customDataView'=>$this->getCustomDataView(),
            'customDataCreate'=>$this->getCustomDataCreate(),
            'customDataAll'=>$this->getCustomDataAll(),
        ];
    }

    function singleDefinition(){
        $id = Request::route("id$this->level");
        $row = $this->getSingleItem($id);
        return [
            'Form ID'=>$this->getHeaderSingular()."-form",

            'Edit Button'=>$this->useEditButton,
            'Edit Header'=>"Edit $this->tableName",
            'Edit Action URL'=>$this->getEditUrl($id),

            'Create Header'=>"Create $this->tableName",
            'Create Action URL'=>$this->getCreateUrl(),

            'Submit Button Text'=>'Save',

            'Inputs' => $this->getInputs()->__invoke($row),
//            'GroupInputs' => $this->getGroupInputs($id)->__invoke($row),
            'customDataEdit'=>$this->getCustomDataEdit(),
            'customDataView'=>$this->getCustomDataView(),
            'customDataCreate'=>$this->getCustomDataCreate(),
            'customDataAll'=>$this->getCustomDataAll(),
            'item' => $row?$row->toArray():null
        ];

    }

    //All

    public function showAll(){
        return ViewUtils::page(
            [
                $this->getCrumbs($this->getHeaderSingular()." Table"),
                $this->showAllView()
            ]);
    }

    public function showAllView(){
        return ViewUtils::box($this->headerPlural,
            [
                \View::make($this->tableTemplate, $this->tableDefinition())
            ]);
    }

    function dataAll(){

        if(isset($this->foreignKeyField)){
            return $this->table->where($this->foreignKeyField,'=',Request::route("id".($this->level-1)))->get()->toArray();
        } else {
            return $this->table->get()->toArray();
        }

    }

    function ajaxAll(){
        return \Response::json(['data'=> $this->dataAll() ]);
    }

    function ajaxDelete(){
        $id = Request::route("id$this->level");
        Log::info($id);
        $this->table->find($id)->delete();
        return \Response::json('success');
    }


    //Create
    function showCreate(){

        $model = [
            'actionUrl'=>$this->getCreateUrl(),
            'form'=>$this->singleDefinition()
        ];
        return
            ViewUtils::page(
                array_merge([
                    $this->getCrumbs($this->getHeaderSingular()." Create"),
                    ViewUtils::box(
                        "Create ".$this->getHeaderSingular(),
                        [
                            \View::make($this->createTemplate,$model)
                                ->with('form',$this->singleDefinition())
                                ->with('actionUrl',$this->getCreateUrl())
                        ])

                ],$this->getCreateViews()));
    }

    function getCrumbs($header,$ps=null){
        return ViewUtils::crumbs($header,$ps);
    }

    function postCreate(){

        $createdId = $this->insert();

        \Session::flash('Success Message','Successfully created');
        return \Redirect::to($this->getCreateReturnUrl($createdId));
    }

    function insert(){
        $form = $this->singleDefinition();
        $row = new $this->table;

        foreach($form['Inputs'] as $input){
            $this->insertItem($row,$input);
        }
        $row->save();

//        foreach($form['GroupInputs'] as $group){
//            foreach($group->children as $c){
//                if($c->doInsert && Input::has($c->id)) {
//                    $group->insertItem($c, $row->id);
//                }
//            }
//            $group->insertAll($row->id);
//        }

        return $row->id;
    }

    function insertItem($row, $input){
        Log::info($input->label);
        Log::info($input->defaultValue);
        if($input->doRender===false && $input->doInsert){
            $input->insert($row, $input->defaultValue);
        } else if($input->doInsert) {
            if (Input::has($input->id) || Request::file($input->id)) {
                $input->insert($row, Input::get($input->id));
            } else {
                $input->insert($row, $input->defaultValue);
            }
        }
    }

    //Edit
    function showEdit(){

        return
            ViewUtils::page([
                    $this->getCrumbs($this->getHeaderSingular()." Edit",Route::current()->parameters()),
                    ViewUtils::box(
                        "Edit ".$this->getHeaderSingular(),
                        [
                            \View::make($this->editTemplate)
                                ->with('form',$this->singleDefinition())
                                ->with('actionUrl', $this->getEditUrl())
                        ])
                ]);
    }

    function postEdit(){
        $this->update();
        \Session::flash('Success Message','Successfully saved');
        return \Redirect::to($this->getEditReturnUrl());
    }

    function update(){

        $id = Request::route("id$this->level");
        $form = $this->singleDefinition();
        $row = $this->table->find($id);
        foreach($form['Inputs'] as $input){

            Log::info($input->label);
            Log::info($input->defaultValue);
            if($input->doRender===false && $input->doInsert){
                $input->update($row, $input->defaultValue);
            } else if($input->doInsert) {

                if (Input::has($input->id) || Request::file($input->id)) {
                    $input->update($row, \Input::get($input->id));
                } else {
                    $input->update($row, $input->nullValue);
                }
            }
        }

        return $row->save();
    }


    function showView(){

        return
            ViewUtils::page(
                array_merge([
                    $this->getCrumbs($this->getHeaderSingular()." View", Route::current()->parameters()),
                    $this->showSingleView()
                ],$this->getViewViews()));
    }

    function showSingleView(){
        $model = [
            'form'=>$this->singleDefinition()
        ];

        return ViewUtils::box(
                    $this->getHeaderSingular(),
                    [
                        \View::make($this->viewTemplate,$model)
                    ]);
    }

    public function allRoute(){
        //All
        \Route::get($this->baseRoute,['as' => $this->getHeaderSingular()." Table", 'uses'=>"$this->controllerClass@showAll"]);
        \Route::get($this->getAjaxRoute(),['as' => $this->getHeaderSingular()." Ajax", 'uses'=> "$this->controllerClass@ajaxAll"]);
        \Route::delete($this->getAjaxDeleteRoute(),"$this->controllerClass@ajaxDelete");
    }

    function createRoute(){
        //Create
        \Route::get($this->getCreateRoute(), ['as' => $this->getHeaderSingular()." Create", 'uses'=>"$this->controllerClass@showCreate"]);
        \Route::post($this->getCreateRoute(), "$this->controllerClass@postCreate");
    }

    function editRoute(){
        //Edit
        \Route::get($this->getEditRoute(), ['as' => $this->getHeaderSingular()." Edit", "uses" => "$this->controllerClass@showEdit"]);
        \Route::post($this->getEditRoute(), "$this->controllerClass@postEdit");
    }

    function viewRoute(){
        //View
        \Route::get($this->getViewRoute(),['as' => $this->getHeaderSingular()." View",  "uses" => "$this->controllerClass@showView"]);
    }



    function currentParamsLess(){
        $ps = Route::current()->parameters();
        $f = array_filter(array_keys($ps), function ($k){ return intval(substr($k,2))<$this->level; });
        $ps = array_intersect_key($ps, array_flip($f));
        return $ps;
    }

    function currentParamsMore(){
        $ps = Route::current()->parameters();
        $f = array_filter(array_keys($ps), function ($k){ return intval(substr($k,2))<=$this->level; });
        $ps = array_intersect_key($ps, array_flip($f));
        return $ps;
    }

    function allBreadcrumb(){
        $parentHeader = $this->parentHeader;
        Breadcrumbs::register($this->getHeaderSingular()." Table", function($breadcrumbs) use($parentHeader) {
            if($parentHeader !== null){
                $breadcrumbs->parent($parentHeader." View");
            }
            $breadcrumbs->push($this->headerPlural, route($this->getHeaderSingular()." Table",$this->currentParamsLess()));

        });
    }

    function createBreadcrumb(){
        $parentHeader = $this->parentHeader;
        Breadcrumbs::register($this->getHeaderSingular()." Create", function($breadcrumbs) use($parentHeader) {

            if($parentHeader===null)
                $breadcrumbs->parent($this->getHeaderSingular()." Table");
            else
                $breadcrumbs->parent($this->parentHeader." View");

            $breadcrumbs->push("Create ".$this->getHeaderSingular(), route($this->getHeaderSingular()." Create",$this->currentParamsLess()));
        });
    }

    function editBreadcrumb(){
        $parentHeader = $this->parentHeader;
        Breadcrumbs::register($this->getHeaderSingular()." Edit", function($breadcrumbs) use($parentHeader) {
            if($parentHeader===null)
                $breadcrumbs->parent($this->getHeaderSingular()." Table");
            else
                $breadcrumbs->parent($this->parentHeader." View");

            $breadcrumbs->push("Edit ".$this->getHeaderSingular(), route($this->getHeaderSingular()." Edit",$this->currentParamsMore()));
        });
    }

    function viewBreadcrumb(){
        $parentHeader = $this->parentHeader;
        Breadcrumbs::register($this->getHeaderSingular()." View", function($breadcrumbs) use($parentHeader) {
            if($parentHeader===null)
                $breadcrumbs->parent($this->getHeaderSingular()." Table");
            else
                $breadcrumbs->parent($this->parentHeader." View");

            $breadcrumbs->push("View ".$this->getHeaderSingular(), route($this->getHeaderSingular()." View",$this->currentParamsMore()));
        });
    }



}