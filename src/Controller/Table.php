<?php namespace MJ1618\AdminUI\Controller;


use Input;
use MJ1618\AdminUI\Utils\ViewUtils;
use Breadcrumbs;

class Table extends BaseTable {


    function getCreateRoute(){                  return "$this->baseRoute/create"; }
    function getEditRoute(){                    return "$this->baseRoute/{id}/edit"; }
    function getBaseUrl(){                      return $this->baseRoute; }
    function getAjaxBaseRoute(){                return $this->ajaxBaseRoute; }
    function getAjaxBaseUrl(){                  return $this->ajaxBaseRoute; }
    function getCreateUrl(){                    return $this->getBaseUrl()."/create"; }

//    function getCreateReturnUrl($id){           return $this->getBaseUrl()."/$id/view"; }
    function getCreateReturnUrl($id){           return $this->getBaseUrl(); }

//    function getEditReturnUrl($id){             return $this->getBaseUrl()."/$id/view"; }
    function getEditReturnUrl($id){             return $this->getBaseUrl(); }

    function getEditPartialRoute(){             return $this->getBaseUrl()."/{id}/edit"; }
    function getEditUrl( $id){                  return $this->getBaseUrl()."/$id/edit";    }
    function getViewPartialRoute(){             return $this->getBaseUrl()."/{id}/view"; }
    function getAjaxUrl(){                      return $this->getAjaxBaseUrl(); }
    function getAjaxRoute(){                    return $this->getAjaxBaseRoute(); }
    function getAjaxDeleteRoute(){              return $this->getAjaxBaseRoute()."/{id}/delete"; }
    function getViewRoute(){                    return $this->getBaseRoute()."/{id}/view";}

    function buttons(){
        return [
            "edit" => [
                'id'=>$this->getHeaderSingular()."-edit",
                'text'=>'Edit',
                'requiresSelect'=>'true',
                'url'=>$this->getEditPartialRoute()
            ],
            "view" => [
                'id'=>$this->getHeaderSingular()."-view",
                'text'=>'View',
                'requiresSelect'=>'true',
                'url'=>$this->getViewPartialRoute()
            ],
            "create" => [
                'id'=>$this->getHeaderSingular()."-create",
                'text'=>'Create',
                'requiresSelect'=>'false',
                'url'=>$this->getCreateUrl(),
                'float'=>'left'
            ]
        ];
    }

    function __construct(){
    }


    function tableDefinition(){
        return [
            'header' => $this->getHeaderSingular(),
            'tableName'=> $this->tableName,
            'attributes' => $this->getAttributes(),
            'buttons'=>$this->buttons(),
            'deleteButton'=>$this->useDeleteButton,
            'ajaxDeletePartialUrl'=>$this->getAjaxDeleteRoute(),
            'ajaxUrl'=>$this->getAjaxUrl(),
            'rows' => $this->useTableAjax ? '' : $this->dataAll(),
            'customDataEdit'=>$this->getCustomDataEdit(),
            'customDataView'=>$this->getCustomDataView(),
            'customDataCreate'=>$this->getCustomDataCreate(),
            'customDataAll'=>$this->getCustomDataAll(),
        ];
    }

    function singleDefinition( $id=''){
        $row = $this->getSingleItem($id);
        return [
            'Form ID'=>$this->getHeaderSingular()."-form",

            'Edit Button'=>$this->useEditButton,
            'Edit Header'=>"Edit $this->tableName",
            'Edit Action URL'=>$this->getEditUrl($id),

            'Create Header'=>"Create $this->tableName",
            'Create Action URL'=>$this->getCreateUrl(),

            'Submit Button Text'=>'Save',

            'Inputs' => $this->getInputs()->__invoke($row),
//            'GroupInputs' => $this->getGroupInputs($id)->__invoke($row),
            'customDataEdit'=>$this->getCustomDataEdit(),
            'customDataView'=>$this->getCustomDataView(),
            'customDataCreate'=>$this->getCustomDataCreate(),
            'customDataAll'=>$this->getCustomDataAll(),
            'item' => $row?$row->toArray():null
        ];

    }

    //All

    public function showAll(){
        return ViewUtils::page(
            [
                ViewUtils::crumbs($this->getHeaderSingular()." Table"),
                $this->showAllView()
            ]);
    }

    public function showAllView(){
        return ViewUtils::box($this->headerPlural,
            [
                \View::make($this->tableTemplate, $this->tableDefinition())
            ]);
    }

    function dataAll(){
        return $this->table->get()->toArray();
    }

    function ajaxAll(){
        return \Response::json(['data'=> $this->dataAll() ]);
    }

    function ajaxDelete($id){
        $this->table->find($id)->delete();
        return \Response::json('success');
    }


    //Create
    function showCreate($views=[]){

        $model = [
            'actionUrl'=>$this->getCreateUrl(),
            'form'=>$this->singleDefinition()
        ];
        return
            ViewUtils::page(
                array_merge([
                    ViewUtils::crumbs($this->getHeaderSingular()." Create"),
                    ViewUtils::box(
                        "Create ".$this->getHeaderSingular(),
                        [
                            \View::make($this->createTemplate,$model)
                                ->with('form',$this->singleDefinition())
                                ->with('actionUrl',$this->getCreateUrl())
                        ])

                ],$views));
    }

    function postCreate(){

        $createdId = $this->insert();

        \Session::flash('Success Message','Successfully created');
        return \Redirect::to($this->getCreateReturnUrl($createdId));
    }

    function insert(){
        $form = $this->singleDefinition();
        $row = new $this->table;

        foreach($form['Inputs'] as $input){
            $this->insertItem($row,$input);
        }
        $row->save();

//        foreach($form['GroupInputs'] as $group){
//            foreach($group->children as $c){
//                if($c->doInsert && Input::has($c->id)) {
//                    $group->insertItem($c, $row->id);
//                }
//            }
//            $group->insertAll($row->id);
//        }

        return $row->id;
    }

    function insertItem($row, $input){
        if($input->doInsert) {
            if (Input::has($input->id)) {
                $input->insert($row, Input::get($input->id));
            } else {
                $input->insert($row, $input->defaultValue);
            }
        }
    }

    //Edit
    function showEdit($id,$views=[]){

        return
            ViewUtils::page(
                array_merge([
                    ViewUtils::crumbs($this->getHeaderSingular()." Edit",$id),
                    ViewUtils::box(
                        "Edit ".$this->getHeaderSingular(),
                        [
                            \View::make($this->editTemplate)
                                ->with('form',$this->singleDefinition($id))
                                ->with('actionUrl', $this->getEditUrl($id))
                        ])
                ],$views));
    }

    function postEdit($id){
        $this->update($id);
        \Session::flash('Success Message','Successfully saved');
        return \Redirect::to($this->getEditReturnUrl($id));
    }

    function update($id){
        $form = $this->singleDefinition($id);
        $row = $this->table->find($id);
        foreach($form['Inputs'] as $input){
            if($input->doInsert) {
                if (Input::has($input->id)) {
                    $input->update($row, \Input::get($input->id));
                }
            }
        }


//        foreach($form['GroupInputs'] as $group){
//            foreach($group->children as $c){
//                if($c->doInsert && Input::has($c->id)) {
//                    $group->updateItem($c, $id);
//                }
//            }
//            $group->updateAll($id);
//        }

        return $row->save();
    }


    function showView($id, $views=[]){

        return
            ViewUtils::page(
                array_merge([
                    ViewUtils::crumbs($this->getHeaderSingular()." View", $id),
                    $this->showSingleView($id)
                ],$views));
    }

    function showSingleView($id){
        $model = [
            'form'=>$this->singleDefinition($id)
        ];

        return ViewUtils::box(
                    $this->getHeaderSingular(),
                    [
                        \View::make($this->viewTemplate,$model)
                    ]);
    }

    public function allRoute(){
        //All
        \Route::get($this->baseRoute,['as' => $this->getHeaderSingular()." Table", 'uses'=>"$this->controllerClass@showAll"]);
        \Route::get($this->getAjaxRoute(), "$this->controllerClass@ajaxAll");
        \Route::delete($this->getAjaxDeleteRoute(),"$this->controllerClass@ajaxDelete");
    }

    function createRoute(){
        //Create
        \Route::get($this->getCreateRoute(), ['as' => $this->getHeaderSingular()." Create", 'uses'=>"$this->controllerClass@showCreate"]);
        \Route::post($this->getCreateRoute(), "$this->controllerClass@postCreate");
    }

    function editRoute(){
        //Edit
        \Route::get($this->getEditRoute(), ['as' => $this->getHeaderSingular()." Edit", "uses" => "$this->controllerClass@showEdit"]);
        \Route::post($this->getEditRoute(), "$this->controllerClass@postEdit");
    }

    function viewRoute(){
        //View
        \Route::get($this->getViewRoute(),['as' => $this->getHeaderSingular()." View",  "uses" => "$this->controllerClass@showView"]);
    }


    function allBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." Table", function($breadcrumbs) {
            $breadcrumbs->push($this->headerPlural, route($this->getHeaderSingular()." Table"));
        });
    }

    function createBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." Create", function($breadcrumbs) {
            $breadcrumbs->parent($this->getHeaderSingular()." Table");
            $breadcrumbs->push("Create ".$this->getHeaderSingular(), route($this->getHeaderSingular()." Create"));
        });
    }

    function editBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." Edit", function($breadcrumbs, $id) {
            $breadcrumbs->parent($this->getHeaderSingular()." Table");
            $breadcrumbs->push("Edit ".$this->getHeaderSingular(), route($this->getHeaderSingular()." Edit",$id));
        });
    }

    function viewBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." View", function($breadcrumbs, $id) {
            $breadcrumbs->parent($this->getHeaderSingular()." Table");
            $breadcrumbs->push("View ".$this->getHeaderSingular(), route($this->getHeaderSingular()." View", $id));
        });
    }



}