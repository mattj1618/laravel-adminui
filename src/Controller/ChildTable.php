<?php namespace MJ1618\AdminUI\Controller;


use Input;
use MJ1618\AdminUI\Utils\ViewUtils;
use Breadcrumbs;

class ChildTable extends BaseTable {


    var $parentHeaderSingular;
    var $foreignKeyField;

    function getCreateRoute(){                  return "$this->baseRoute/create"; }
    function getEditRoute(){                    return "$this->baseRoute/{id}/edit"; }
    public function getParentHeaderSingular(){  return $this->parentHeaderSingular;}
    function getAjaxBaseRoute(){                return $this->ajaxBaseRoute; }
    function getAjaxBaseUrl($parentId){         return str_replace('{parentId}',$parentId,$this->ajaxBaseRoute); }
    function getBaseUrl($parentId){             return str_replace('{parentId}',$parentId,$this->baseRoute); }
    function getCreateUrl($parentId){           return $this->getBaseUrl($parentId)."/create"; }

//    function getCreateReturnUrl($parentId,
//                                $createdId ){   return $this->getBaseUrl($parentId)."/$createdId/view"; }
    function getCreateReturnUrl($parentId,
                                $createdId ){   return route($this->parentHeaderSingular.' View', ['id'=>$parentId]); }

//    function getEditReturnUrl($parentId, $id){  return $this->getBaseUrl($parentId)."/$id/view"; }
    function getEditReturnUrl($parentId, $id){  return route($this->parentHeaderSingular.' View', ['id'=>$parentId]); }

    function getEditPartialRoute($parentId){    return $this->getBaseUrl($parentId)."/{id}/edit"; }
    function getEditUrl($parentId, $id){        return $this->getBaseUrl($parentId)."/$id/edit";    }
    function getViewPartialRoute($parentId){    return $this->getBaseUrl($parentId)."/{id}/view"; }
    function getAjaxUrl($parentId){             return $this->getAjaxBaseUrl($parentId); }
    function getAjaxRoute(){                    return $this->getAjaxBaseRoute(); }
    function getAjaxDeleteRoute(){              return $this->getAjaxBaseRoute()."/{id}/delete"; }
    function getAjaxDeletePartialUrl($parentId){return $this->getAjaxBaseUrl($parentId)."/{id}/delete"; }
    function getViewRoute(){                    return $this->getBaseRoute()."/{id}/view";}

    function buttons($parentId){
        return [
            'edit' => [
                'id'=>$this->getHeaderSingular()."-edit",
                'text'=>'Edit',
                'requiresSelect'=>'true',
                'url'=>$this->getEditPartialRoute($parentId)
            ],
            "view" => [
                'id'=>$this->getHeaderSingular()."-view",
                'text'=>'View',
                'requiresSelect'=>'true',
                'url'=>$this->getViewPartialRoute($parentId)
            ],
            "create" => [
                'id'=>$this->getHeaderSingular()."-create",
                'text'=>'Create',
                'requiresSelect'=>'false',
                'url'=>$this->getCreateUrl($parentId),
                'float'=>'left'
            ]
        ];
    }

    function __construct(){
    }


    function tableDefinition($parentId){
        return [
            'header' => $this->getHeaderSingular(),
            'tableName'=> $this->tableName,
            'attributes' => $this->getAttributes(),
            'buttons'=>$this->buttons($parentId),
            'deleteButton'=>$this->useDeleteButton,
            'ajaxDeletePartialUrl'=>$this->getAjaxDeletePartialUrl($parentId),
            'ajaxUrl'=>$this->getAjaxUrl($parentId),
            'rows' => $this->useTableAjax ? [] : $this->dataAll($parentId),
            'customDataTable'=>$this->getCustomDataTable(),
            'customDataAll'=>$this->getCustomDataAll()
        ];
    }

    function singleDefinition($parentId, $id=''){
        $row = $this->getSingleItem($id,$parentId);
        return [
            'Form ID'=>$this->getHeaderSingular()."-form",

            'Edit Header'=>"Edit $this->tableName",
            'Edit Action URL'=>$this->getEditUrl($parentId,$id),

            'Create Header'=>"Create $this->tableName",
            'Create Action URL'=>$this->getCreateUrl($parentId),

            'Submit Button Text'=>'Save',

            'Inputs' => $this->getInputs()->__invoke($row,$parentId),
            'customDataEdit'=>$this->getCustomDataEdit(),
            'customDataView'=>$this->getCustomDataView(),
            'customDataCreate'=>$this->getCustomDataCreate(),
            'customDataAll'=>$this->getCustomDataAll(),

            'item' => $row?$row->toArray():''
        ];
    }

    //All

    public function showAll($parentId){

        return ViewUtils::page(
            [
                ViewUtils::crumbs($this->getHeaderSingular()." Table", $parentId),
                $this->showAllView($parentId)
            ]);
    }

    public function showAllView($parentId){
        return ViewUtils::box($this->headerPlural,
            [
                \View::make($this->tableTemplate, $this->tableDefinition($parentId))
            ]);
    }

    function dataAll($parentId){
        return $this->table->where($this->foreignKeyField,"=",$parentId)->get()->toArray();
    }

    function ajaxAll($parentId){
        return \Response::json(['data'=> $this->dataAll($parentId) ]);
    }

    function ajaxDelete($parentId, $id){
        $this->table->find($id)->delete();
        return \Response::json('success');
    }


    //Create
    function showCreate($parentId,$views=[]){
        $model = [
            'actionUrl'=>$this->getCreateUrl($parentId),
            'form'=>$this->singleDefinition($parentId)
        ];

        return
            ViewUtils::page(
                array_merge([
                    ViewUtils::crumbs($this->getHeaderSingular()." Create",$parentId),
                    ViewUtils::box(
                        "Create ".$this->getHeaderSingular(),
                        [
                            \View::make($this->createTemplate,$model)
                                ->with('form',$this->singleDefinition($parentId))
                                ->with('actionUrl',$this->getCreateUrl($parentId))
                        ])

                ],$views));
    }

    function postCreate($parentId){

        $createdId = $this->insert($parentId);


        \Session::flash('Success Message','Successfully created');
        return \Redirect::to($this->getCreateReturnUrl($parentId, $createdId));
    }

    function insert($parentId){
        $form = $this->singleDefinition($parentId);
        $row = new $this->table;
        foreach($form['Inputs'] as $input){

            if($input->doInsert) {
                if (Input::has($input->id)) {
                    $input->insert($row, \Input::get($input->id));
                } else {
                    if($input->defaultValue!=='')
                        $input->insert($row, $input->defaultValue);
                }
            }
        }

        $row->save();


        $this->updateForeignKeyField($row,$parentId);


        return $row->id;
    }

    function updateForeignKeyField($row, $parentId)
    {
        $row->{$this->foreignKeyField}=$parentId;
        $row->save();
    }

    //Edit
    function showEdit($parentId,$id,$views=[]){

        return
            ViewUtils::page(
                array_merge([
                    ViewUtils::crumbs($this->getHeaderSingular()." Edit",$parentId,$id),
                    ViewUtils::box(
                        "Edit ".$this->getHeaderSingular(),
                        [
                            \View::make($this->editTemplate)
                                ->with('form',$this->singleDefinition($parentId,$id))
                                ->with('actionUrl', $this->getEditUrl($parentId,$id))
                        ])

                ],$views));
    }

    function postEdit($parentId,$id){

        $this->update($parentId,$id);
        \Session::flash('Success Message','Successfully saved');
        return \Redirect::to($this->getEditReturnUrl($parentId,$id));
    }

    function update($parentId,$id){
        $form = $this->singleDefinition($parentId,$id);
        $row = $this->table->find($id);
        foreach($form['Inputs'] as $input){
            if($input->doInsert) {
                if (Input::has($input->id)) {
                    $input->update($row, \Input::get($input->id));
                }
            }
        }
        return $row->save();
    }

    function showView($parentId, $id){
        $model = [
            'form'=>$this->singleDefinition($parentId,$id)
        ];
        return
            ViewUtils::page(
                [
                    ViewUtils::crumbs($this->getHeaderSingular()." View", $parentId, $id),
                    ViewUtils::box(
                        $this->getHeaderSingular(),
                        [
                            \View::make($this->viewTemplate,$model)
                        ])
                ]);
    }





    function allRoute(){
        //All
        \Route::get($this->getBaseRoute(),['as' => $this->getHeaderSingular()." Table", 'uses'=>"$this->controllerClass@showAll"]);
        \Route::get($this->getAjaxRoute(), "$this->controllerClass@ajaxAll");
        \Route::delete($this->getAjaxDeleteRoute(),"$this->controllerClass@ajaxDelete");
    }

    function editRoute(){
        //Edit
        \Route::get($this->getEditRoute(), ['as' => $this->getHeaderSingular()." Edit", "uses"=>"$this->controllerClass@showEdit"]);
        \Route::post($this->getEditRoute(), "$this->controllerClass@postEdit");
    }

    function createRoute(){
        //Create
        \Route::get($this->getCreateRoute(), ['as' => $this->getHeaderSingular()." Create", 'uses'=>"$this->controllerClass@showCreate"]);
        \Route::post($this->getCreateRoute(), "$this->controllerClass@postCreate");
    }

    function viewRoute(){
        //View
        \Route::get($this->getViewRoute(),['as' => $this->getHeaderSingular()." View",  "uses"=>"$this->controllerClass@showView"]);
    }




    function allBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." Table", function($breadcrumbs, $parentId) {
            $breadcrumbs->parent($this->getParentHeaderSingular()." View", $parentId);
            $breadcrumbs->push($this->headerPlural, route($this->getHeaderSingular()." Table",$parentId));
        });
    }

    function createBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." Create", function($breadcrumbs, $parentId) {
            $breadcrumbs->parent($this->getParentHeaderSingular()." View", $parentId);
            $breadcrumbs->push("Create ".$this->getHeaderSingular(), route($this->getHeaderSingular()." Create"));
        });
    }

    function editBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." Edit", function($breadcrumbs, $parentId, $id) {
            $breadcrumbs->parent($this->getHeaderSingular()." View",$parentId, $id);
            $breadcrumbs->push("Edit ".$this->getHeaderSingular(), route($this->getHeaderSingular()." Edit",$id));
        });
    }

    function viewBreadcrumb(){
        Breadcrumbs::register($this->getHeaderSingular()." View", function($breadcrumbs, $parentId, $id) {
            $breadcrumbs->parent($this->getParentHeaderSingular()." View", $parentId);
            $breadcrumbs->push("View ".$this->getHeaderSingular(), route($this->getHeaderSingular()." View", [$parentId, $id]));
        });
    }

    public function parentHeaderSingular($parentHeaderSingular)
    {
        $this->parentHeaderSingular = $parentHeaderSingular;
    }

    public function foreignKeyField($foreignKeyField)
    {
        $this->foreignKeyField = $foreignKeyField;
    }



}