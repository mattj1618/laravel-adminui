<?php namespace MJ1618\AdminUI\Controller;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use MJ1618\AdminUI\Form\DateRange;
use MJ1618\AdminUI\Form\DateTimeRange;
use MJ1618\AdminUI\Form\DropDown;
use MJ1618\AdminUI\Form\MetaItem;
use MJ1618\AdminUI\Form\TextAreaBox;
use MJ1618\AdminUI\Utils\ViewUtils;
use Illuminate\Support\Facades\Log;


class ListController extends Controller {

    var $template = 'admin-ui::list';
    var $controllerClass;
    var $header;
    var $parentBreadcrumb;
    var $baseRoute;
    var $plainPage = false;
    var $infoMessages=[];

    function getUrl(){ return $this->baseRoute; }


    function beforeShow(){
        return null;
    }

    //Create
    function show(){

        $b = $this->beforeShow();
        if($b!==null)return $b;

        return $this->showView();
    }

    function showView(){

        $allViews = [
            $this->getCrumbs(),
            ViewUtils::box(
                $this->getHeader(),[
                    ViewUtils::listView(
                        $this->definition())
                ])
        ];

        if($this->plainPage===true){
            return ViewUtils::plainPage($allViews);
        } else {
            return ViewUtils::page($allViews);
        }
    }

    function routes(){
        $this->showRoute();
    }

    function breadcrumbs(){
        $this->showBreadcrumb();
    }

    function showRoute(){
        Route::get($this->baseRoute, ['as' => $this->getHeader(), 'uses'=>"$this->controllerClass@show"]);
    }

    function showBreadcrumb(){
        \Breadcrumbs::register($this->getHeader(), function($breadcrumbs, $parentId) {
            $breadcrumbs->push($this->getHeader(), route($this->getHeader()));
        });
    }



    function definition($def=[]){
        return array_merge($def,[
            'Info Messages' => $this->infoMessages
        ]);
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return mixed
     */
    public function getParentBreadcrumb()
    {
        return $this->parentBreadcrumb;
    }

    function getCrumbs()
    {
        return ViewUtils::crumbs($this->getHeader());
    }


}