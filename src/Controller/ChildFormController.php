<?php namespace MJ1618\AdminUI\Controller;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use MJ1618\AdminUI\Form\DateRange;
use MJ1618\AdminUI\Form\DateTimeRange;
use MJ1618\AdminUI\Form\DropDown;
use MJ1618\AdminUI\Form\MetaItem;
use MJ1618\AdminUI\Form\TextAreaBox;
use MJ1618\AdminUI\Utils\ViewUtils;
use Illuminate\Support\Facades\Log;


class ChildFormController extends Controller {

    var $template = 'admin-ui::table/edit';
    var $controllerClass;
    var $header;
    var $parentBreadcrumb;
    var $baseRoute;

    function getUrl($parentId){ return str_replace('{parentId}',$parentId,$this->baseRoute); }



    //Create
    function show($parentId,$views=[]){
        $model = [
            'actionUrl'=>$this->getUrl($parentId).'/form',
            'form'=>$this->definition($parentId)
        ];

        return
            ViewUtils::page(
                array_merge([
                    ViewUtils::crumbs($this->getHeader(),$parentId),
                    ViewUtils::box(
                        $this->getHeader(),
                        [
                            View::make($this->template,$model)
                                ->with('form',$this->definition($parentId))
                                ->with('actionUrl',$this->getUrl($parentId).'/form')
                        ])

                ],$views));
    }



    function post($parentId){
        return Redirect::to($this->getUrl($parentId));
    }

    function routes(){
        $this->showRoute();
    }

    function breadcrumbs(){
        $this->showBreadcrumb();
    }

    function showRoute(){
        Route::get($this->baseRoute.'/form', ['as' => $this->getHeader(), 'uses'=>"$this->controllerClass@show"]);
        Route::post($this->baseRoute.'/form', "$this->controllerClass@post");
    }

    function showBreadcrumb(){
        \Breadcrumbs::register($this->getHeader(), function($breadcrumbs, $parentId) {
            $breadcrumbs->parent($this->getParentBreadcrumb(), $parentId);
            $breadcrumbs->push($this->getHeader(), route($this->getHeader()));
        });
    }



    function definition($parentId){
        return [];
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return mixed
     */
    public function getParentBreadcrumb()
    {
        return $this->parentBreadcrumb;
    }


}