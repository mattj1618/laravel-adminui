<?php namespace MJ1618\AdminUI\Controller;


use Input;
use MJ1618\AdminUI\Utils\ViewUtils;
use Breadcrumbs;
use View;
use Route;
use Response;
class BasicTable extends Controller{

    var $route = '';
    var $controllerClass='BasicTable';
    var $header='table';

    var $rows = [];
    var $row = [];
    var $template = "admin-ui::basic-table";

    function header(){
        return $this->header;
    }

    function addCell($value){
        $this->row["cols"][] = [
            "value" => $value
        ];
    }
    function addCellView($view){
        $this->row["cols"][] = [
            "view" => $view,
            "isView"=>true
        ];
    }
    function addCellViews($views){
        $this->row["cols"][] = [
            "views" => $views,
            "isViews"=>true
        ];
    }
    function firstRow($h=''){
        $this->row = [
            "header"=>$h,
            "cols"=> []
        ];
    }

    function nextRow($h=''){
        $this->rows[] = $this->row;
        $this->row = [
            "header"=>$h,
            "cols"=> []
        ];
    }

    function definition(){
        return [
            'id'=>$this->header,
            'rows'=>$this->rows,
            'print'=>null,
            'export'=>null
        ];
    }

    function export(){
        $headers = array(
            'Pragma' => 'public',
            'Expires' => 'public',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control' => 'private',
            'Content-Type' => 'text/xls',
            'Content-Disposition' => 'attachment; filename=download.xls',
            'Content-Transfer-Encoding' => ' binary'
        );
        return Response::make($this->showTable()->render(), 200, $headers);
    }

    function show(){
        return ViewUtils::page(
            [
                $this->showBox()
            ]);
    }

    function showPrint(){
        return ViewUtils::printPage(
            [
                $this->showTable()
            ]);
    }

    function showBox(){

        return ViewUtils::box($this->header(),
            [
                $this->showTable()
            ]);
    }

    function showTable(){
        return View::make($this->template, ["table"=> $this->definition()]);
    }

    function routes(){
        Route::get($this->route, "$this->controllerClass@show");
        Route::get($this->route."/print", "$this->controllerClass@showPrint");
        Route::get($this->route."/export", "$this->controllerClass@export");
    }

}