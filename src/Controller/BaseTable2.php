<?php namespace MJ1618\AdminUI\Controller;


use MJ1618\AdminUI\Utils\ViewUtils;
use Illuminate\Support\Facades\Log;
use Request;
use Route;


class BaseTable2 extends Controller {
    var $table;
    var $baseRoute = '';
    var $ajaxBaseRoute = '';
    var $headerSingular = '';
    var $headerPlural = '';
    var $tableName = '';
    var $attributes = [];
    var $inputs = null;
    var $groupInputs = null;
    var $controllerClass = '';
    var $tableTemplate = 'admin-ui::table/table';
    var $viewTemplate = 'admin-ui::table/view';
    var $editTemplate = 'admin-ui::table/edit';
    var $createTemplate = 'admin-ui::table/edit';
    var $useTableAjax = true;
    var $useDeleteButton=true;
    var $useEditButton=true;

    var $customDataTable = [];
    var $customDataView = [];
    var $customDataEdit = [];
    var $customDataCreate = [];
    var $customDataAll=[];
    var $parentHeader=null;
    var $level=1;

    var $foreignKeyField=null;

    function getNameField(){
        return 'name';
    }

    function getCreateViews(){
        return [];
    }
    function getEditViews(){
        return [];
    }
    function getViewViews(){
        return [];
    }

    public function routes(){
        $this->allRoute();
        $this->createRoute();
        $this->editRoute();
        $this->viewRoute();
    }
    function breadcrumbs(){
        $this->allBreadcrumb();
        $this->createBreadcrumb();
        $this->editBreadcrumb();
        $this->viewBreadcrumb();
    }

    function getSingleItem(){
        $id = Request::route("id$this->level");
        return $this->table->find($id);
    }

    function allRoute(){

    }
    function createRoute(){

    }
    function editRoute(){

    }
    function viewRoute(){

    }
    function allBreadcrumb(){

    }
    function createBreadcrumb(){

    }
    function editBreadcrumb(){

    }
    function viewBreadcrumb(){

    }


    public function getTable()
    {
        return $this->table;
    }

    public function table($table)
    {
        $this->table = $table;
    }

    public function getBaseRoute()
    {
        return $this->baseRoute;
    }

    public function baseRoute($baseRoute)
    {
        $this->baseRoute = $baseRoute;
    }

    public function getAjaxBaseRoute()
    {
        return $this->ajaxBaseRoute;
    }

    public function ajaxBaseRoute($ajaxBaseRoute)
    {
        $this->ajaxBaseRoute = $ajaxBaseRoute;
    }

    public function getHeaderSingular()
    {
        return $this->headerSingular;
    }

    public function headerSingular($headerSingular)
    {
        $this->headerSingular = $headerSingular;
    }

    public function getHeaderPlural()
    {
        return $this->headerPlural;
    }

    public function headerPlural($headerPlural)
    {
        $this->headerPlural = $headerPlural;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function tableName($tableName)
    {
        $this->tableName = $tableName;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function attributes($attributes)
    {
        $this->attributes = $attributes;
    }

    public function getInputs()
    {
        if($this->inputs===null){
            return function($row){
                return [];
            };
        }
        return $this->inputs;
    }

    public function inputs($inputs)
    {
        $this->inputs = $inputs;
    }

    public function getGroupInputs()
    {
        if($this->groupInputs===null){
            return function($row){
                return [];
            };
        }
        return $this->groupInputs;
    }

    public function groupInputs($groupInputs)
    {
        $this->groupInputs = $groupInputs;
    }
    public function getControllerClass()
    {
        return $this->controllerClass;
    }

    public function controllerClass($controllerClass)
    {
        $this->controllerClass = $controllerClass;
    }

    public function getTableTemplate()
    {
        return $this->tableTemplate;
    }

    public function tableTemplate($tableTemplate)
    {
        $this->tableTemplate = $tableTemplate;
    }

    public function getViewTemplate()
    {
        return $this->viewTemplate;
    }

    public function viewTemplate($viewTemplate)
    {
        $this->viewTemplate = $viewTemplate;
    }

    public function getEditTemplate()
    {
        return $this->editTemplate;
    }

    public function editTemplate($editTemplate)
    {
        $this->editTemplate = $editTemplate;
    }

    public function getCreateTemplate()
    {
        return $this->createTemplate;
    }

    public function createTemplate($createTemplate)
    {
        $this->createTemplate = $createTemplate;
    }

    public function isUseTableAjax()
    {
        return $this->useTableAjax;
    }

    public function useTableAjax($useTableAjax)
    {
        $this->useTableAjax = $useTableAjax;
    }

    public function getCustomDataTable()
    {
        return $this->customDataTable;
    }

    public function customDataTable($customDataTable)
    {
        $this->customDataTable = $customDataTable;
    }

    public function getCustomDataView()
    {
        return $this->customDataView;
    }

    public function customDataView($customDataView)
    {
        $this->customDataView = $customDataView;
    }

    public function getCustomDataEdit()
    {
        return $this->customDataEdit;
    }

    public function customDataEdit($customDataEdit)
    {
        $this->customDataEdit = $customDataEdit;
    }

    public function getCustomDataCreate()
    {
        return $this->customDataCreate;
    }

    public function customDataCreate($customDataCreate)
    {
        $this->customDataCreate = $customDataCreate;
    }

    public function getCustomDataAll()
    {
        return $this->customDataAll;
    }

    public function customDataAll($customDataAll)
    {
        $this->customDataAll = $customDataAll;
    }


}