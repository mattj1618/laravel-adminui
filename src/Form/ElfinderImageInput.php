<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class ElfinderImageInput extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/elfinder-image", ['item' => $this]);
    }
    function renderView() {
        return \View::make("admin-ui::form/view-elfinder-image", ['item' => $this]);
    }

}