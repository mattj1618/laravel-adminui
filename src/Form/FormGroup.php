<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;
use MJ1618\AdminUI\Utils\ViewUtils;

class FormGroup  extends FormInput {

    function __construct(){
        $this->isGroup(true);
    }

    function insertItem($c, $parentId){

    }

    function updateItem($c, $parentId){

    }

    function insertAll(){

    }

    function render()
    {
        $views = [];

        foreach($this->children as $c){
            $views[] = $c->render();
        }

        return ViewUtils::plain($views);
    }

    function renderView()
    {
        $views = [];

        foreach($this->children as $c){
            $views[] = $c->renderView();
        }

        return ViewUtils::plain($views);
    }

}
