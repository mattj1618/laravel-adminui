<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

 class PlainTextAreaBox  extends FormInput {

     function __construct(){
         $this->isRow=true;
     }

     function render()
     {
         return View::make("admin-ui::form/plaintextarea", ['item' => $this]);
     }

 }
