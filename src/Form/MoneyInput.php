<?php namespace MJ1618\AdminUI\Form;

use Illuminate\Support\Facades\View;

class MoneyInput extends FormInput {

    function parse($s){
        if(strlen($s)===0)
            return null;
        else return $s;
    }

    function render() {
        return View::make("admin-ui::form/money", ['item' => $this]);
    }
}
