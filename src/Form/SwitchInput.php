<?php namespace MJ1618\AdminUI\Form;

use Illuminate\Support\Facades\View;

class SwitchInput extends FormInput {

    var $on = "Yes";
    var $off = "No";

    public function on($on)
    {
        $this->on = $on;
        return $this;
    }

    public function off($off)
    {
        $this->off = $off;
        return $this;
    }



    public function render(){

        return View::make("admin-ui::form/switch", ['item' => $this]);
    }

//    function renderView() {
//
//        return \View::make("admin-ui::form/view-switch", ['item' => $this]);
//    }


    function defaultValue($d,$defaultValue2 = NULL){
        if($d==='1' || $d===1){
            $this->defaultValue=1;
        } else {
            $this->defaultValue=0;
        }
        return $this;
    }

    function insert($row, $value){
//        dd('val:'.$value);
        parent::insert($row, $value==='false'?0:1);
        return $row;
    }

    function update($row, $value){
//        dd('val:'.$value);
        parent::update($row, $value==='false'?0:1);
        return $row;
    }


}

