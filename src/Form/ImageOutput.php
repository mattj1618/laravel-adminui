<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Request;
use Input;
use Log;
use Image;
class ImageOutput extends FormInput {
    function render()
    {
        return View::make("admin-ui::form/image-output", ['item' => $this]);
    }
    function renderView()
    {
        return View::make("admin-ui::form/image-output", ['item' => $this]);
    }

}