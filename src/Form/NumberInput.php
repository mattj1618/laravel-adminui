<?php namespace MJ1618\AdminUI\Form;

use Illuminate\Support\Facades\View;

class NumberInput extends FormInput {
    function render() {
        return View::make("admin-ui::form/number", ['item' => $this]);
    }
}
