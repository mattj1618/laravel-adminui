<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class HeaderItem extends FormInput {

    var $style;

    public function style($style)
    {
        $this->style = $style;
        return $this;
    }
    
    function __construct(){
        $this->doInsert(false);
        $this->isRow(true);
        $this->lgCols = 12;
    }

    function render()
    {
        return View::make("admin-ui::form/header", ['item' => $this]);
    }

    function renderView()
    {
        return View::make("admin-ui::form/header", ['item' => $this]);
    }

    function insert($row, $value){}
    function update($row, $value){}
}