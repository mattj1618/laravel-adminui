<?php namespace MJ1618\AdminUI\Form;


use Config;
use Illuminate\Support\Facades\View;

class ButtonItem extends FormInput {

    var $cssClass;
    var $target=null;
    public function cssClass($cssClass=null,$target=null)
    {
        $this->cssClass = $cssClass;
        $this->target=$target;
        return $this;
    }

    public function target($target)
    {
        $this->target = $target;
        return $this;
    }



    function __construct(){
        $this->cssClass='btn-'.Config::get('admin-ui.default-btn-type');
    }

    function render()
    {
        return View::make("admin-ui::form/button", ['item' => $this]);
    }

}