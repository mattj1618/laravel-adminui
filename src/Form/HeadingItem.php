<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class HeadingItem extends FormInput {

    var $views;

    function __construct(){
        $this->doInsert(false);
        $this->isRow(true);
        $this->lgCols = 12;
    }

    public function views($views)
    {
        $this->views = $views;
        return $this;
    }



    function render()
    {
        return View::make("admin-ui::form/heading", ['item' => $this]);
    }

    function renderView()
    {
        return View::make("admin-ui::form/heading", ['item' => $this]);
    }

    function insert($row, $value){}
    function update($row, $value){}
}