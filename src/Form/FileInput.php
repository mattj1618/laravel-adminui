<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Request;
use Input;
use Log;
class FileInput extends FormInput {
    var $filenameField = null;
    var $filename = '';

    var $useOriginalName=false;

    public function useOriginalName($useOriginalName)
    {
        $this->useOriginalName = $useOriginalName;
        return $this;
    }


    function renderView() {
        return \View::make("admin-ui::form/view-file", ['item' => $this]);
    }

    function filename($fn){
        $this->filename=$fn;
        return $this;
    }

    public function filenameField($filenameField)
    {
        $this->filenameField = $filenameField;
        return $this;
    }
    function insert($row, $value){
        $file = Request::file("$this->id");
        if(isset($file)) {
            if($this->useOriginalName)
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),$file->getClientOriginalName());
            else
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),str_random(10).'.'.$file->getClientOriginalExtension());
            parent::insert($row, $url);
            if(isset($this->filenameField))
                $row->{$this->filenameField} = $file->getClientOriginalName();
        }
        return $row;
    }

    function update($row, $value){
        $file = Request::file("$this->id");
        if(isset($file)) {
            if($this->useOriginalName)
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),$file->getClientOriginalName());
            else
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),str_random(10).'.'.$file->getClientOriginalExtension());

            parent::insert($row, $url);
            if(isset($this->filenameField))
                $row->{$this->filenameField} = $file->getClientOriginalName();
        }
        return $row;
    }

    function render()
    {
        return View::make("admin-ui::form/file", ['item' => $this]);
    }

}