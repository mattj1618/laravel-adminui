<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class StripeInput extends FormInput {

    var $name='';
    var $description='';
    var $amount='';
    var $formId=null;

    function render()
    {
        return View::make("admin-ui::form/stripe-custom", ['item' => $this]);
    }

    public function formId($formId)
    {
        $this->formId = $formId;
        return $this;
    }



    public function name($name)
    {
        $this->name = $name;
        return $this;
    }

    public function description($description)
    {
        $this->description = $description;
        return $this;
    }

    public function amount($amount)
    {
        $this->amount = $amount;
        return $this;
    }


}