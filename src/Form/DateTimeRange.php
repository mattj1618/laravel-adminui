<?php namespace MJ1618\AdminUI\Form;


use DateTime;
use Illuminate\Support\Facades\View;
use Log;

class DateTimeRange extends FormInput {

    var $idStart;
    var $idEnd;
    var $startDate;
    var $endDate;

    function __construct(){
        $this->defaultValue = date('Y-m-d H:i:s',time());
    }
    public function render(){
        $value =  explode(" - ",$this->defaultValue);

        $start = '';
        $end = '';

        if(count($value)!==2){
            $start = new DateTime();
            $end = new DateTime();
        } else {
            $start = DateTime::createFromFormat('Y-m-d H:i:s', $value[0]);
            $end = DateTime::createFromFormat('Y-m-d H:i:s', $value[1]);
        }

        if($start instanceof DateTime){
            $this->startDate = $start->format('F j, Y H:i A');
        }

        if($end instanceof DateTime){
            $this->endDate = $end->format('F j, Y H:i A');
        }

        return View::make("admin-ui::form/datetimerange", ['item' => $this]);
    }

    public function renderView(){

        $dr = clone $this;

        Log::info("date time range : $this->defaultValue");

        $value =  explode(" - ",$this->defaultValue);
        $start = ''.DateTime::createFromFormat('Y-m-d H:i:s', $value[0])->format('F j, Y H:i A');
        $end = ''.DateTime::createFromFormat('Y-m-d H:i:s', $value[1])->format('F j, Y H:i A');
        $dr->defaultValue="$start - $end";
        return View::make("admin-ui::form/view", ['item' => $dr]);
    }




    function insert($row, $v){
        $value = explode(" - ",$v);
        $row->{$this->idStart} = ''.DateTime::createFromFormat('F j, Y H:i A', $value[0])->format('Y-m-d H:i:s');
        $row->{$this->idEnd} = ''.DateTime::createFromFormat('F j, Y H:i A', $value[1])->format('Y-m-d H:i:s');
    }

    function update($row, $v){
        $value = explode(" - ",$v);
        $row->{$this->idStart} = ''.DateTime::createFromFormat('F j, Y H:i A', $value[0])->format('Y-m-d H:i:s');
        $row->{$this->idEnd} = ''.DateTime::createFromFormat('F j, Y H:i A', $value[1])->format('Y-m-d H:i:s');
    }

    public function idStart($idStart)
    {
        $this->idStart = $idStart;
        return $this;
    }

    public function idEnd($idEnd)
    {
        $this->idEnd = $idEnd;
        return $this;
    }
//
//    String idStart;
//    String idEnd;
//    String label;
//    String defaultText = new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(new Date())+" - "+new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(new Date());
//
//
//    @Override
//    public String render() {
//
//        Map<String,Object> model = new HashMap<String, Object>();
//        model.put("date",this);
//        return Velocity.engine.render(new ModelAndView(model, "/assets/vms/form/datetimerange.vm"));
//    }
//    public String label() {
//        return this.label;
//    }
//
//    public String defaultText() {
//        return this.defaultText;
//    }
//
//
//    public DateTimeRange label(final String label) {
//        this.label = label;
//        return this;
//    }
//
//    public DateTimeRange defaultText(final String defaultText) {
//        this.defaultText = defaultText;
//        return this;
//    }
//
//    public String idStart() {
//        return this.idStart;
//    }
//
//    public String idEnd() {
//        return this.idEnd;
//    }
//
//    public DateTimeRange idStart(final String idStart) {
//        this.idStart = idStart;
//        return this;
//    }
//
//    public DateTimeRange idEnd(final String idEnd) {
//        this.idEnd = idEnd;
//        return this;
//    }
//
//    public String id(){
//        return idStart+"-"+idEnd;
//    }
//
//    public DateTimeRange defaultDate(Row row) {
//        if(row!=null&&row.contains(idStart)&&row.contains(idEnd))
//            this.defaultText=new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(new Date(row.time(idStart).getTime()))+" - "+new SimpleDateFormat("dd/MM/yyyy hh:mm a").format(new Date(row.time(idEnd).getTime()));
//        return this;
//    }
//
//    @Override
//    public boolean delegateSql(){
//        return true;
//    }
//
//    @Override public void insert(Table t, String value){
//
//
//        try {
//            String[] ds = value.split(" - ");
//            t.insert(idStart, new java.sql.Timestamp(new SimpleDateFormat("dd/MM/yyyy hh:mm a").parse(ds[0]).getTime()));
//            t.insert(idEnd, new java.sql.Timestamp(new SimpleDateFormat("dd/MM/yyyy hh:mm a").parse(ds[1]).getTime()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override public void update(Table t, String value){
//
//        try {
//            String[] ds = value.split(" - ");
//            t.update(idStart, new java.sql.Timestamp(new SimpleDateFormat("dd/MM/yyyy hh:mm a").parse(ds[0]).getTime()));
//            t.update(idEnd, new java.sql.Timestamp(new SimpleDateFormat("dd/MM/yyyy hh:mm a").parse(ds[1]).getTime()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
