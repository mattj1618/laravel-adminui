<?php namespace MJ1618\AdminUI\Form;



class PlainDropDown extends DropDown {

    public function render(){
        return \View::make("admin-ui::form/plain-dropdown", ['item' => $this]);
    }


}
