<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;
class HiddenInput extends FormInput {
    function render() {
        return View::make("admin-ui::form/hidden", ['item' => $this]);
    }

    function renderView(){

    }

}
