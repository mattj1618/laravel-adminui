<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class TextBox extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/textbox", ['item' => $this]);
    }

}