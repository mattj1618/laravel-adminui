<?php namespace MJ1618\AdminUI\Form;



use Illuminate\Support\Facades\Log;
use Purifier;
abstract class FormInput {

    public $id;
    public $label;
    public $defaultValue;
    public $nullValue=null;
    public $disabled=false;
    public $doInsert=true;
    public $isGroup = false;
    public $children = [];
    public $isRow=false;
    public $valueField = null;

    public $lgCols = 6;

    public $doRender=true;


    abstract function render();

    function renderView() {
        return \View::make("admin-ui::form/view", ['item' => $this]);
    }

    function renderView6() {
        return $this->renderView();
    }
    function renderView12() {
        $this->lgCols(12);
        return $this->renderView();
    }

    function parse($s){
        return $s;
    }

    function parseMulti($s){
        return implode(',',$s);
    }

    function purify($s){
        return Purifier::clean($s);
    }

    function insert($row, $value){
        if($this->valueField===null){
            if($value===null){
                $row->{$this->id} = null;
            } else {
                $row->{$this->id} = $this->purify($this->parse($value));
            }

        } else{
            if($value===null){
                $row->{$this->valueField} = null;
            } else {
                $row->{$this->valueField} = $this->purify($this->parse($value));
            }
        }
        return $row;
    }

    function update($row, $value){

        if($this->valueField===null){
            if($value===null){
                $row->{$this->id} = null;
            } else {
                $row->{$this->id} = $this->purify($this->parse($value));
            }

        } else{
            if($value===null){
                $row->{$this->valueField} = null;
            } else {
                $row->{$this->valueField} = $this->purify($this->parse($value));
            }
        }
        return $row;
    }

    public function id($id)
    {
        $this->id = $id;
        return $this;
    }

    public function label($label)
    {
        $this->label = $label;
        return $this;
    }

    public function defaultValue($defaultValue, $defaultValue2=null)
    {

        $this->defaultValue = $defaultValue;

//        dd($defaultValue);
        return $this;
    }

    public function disabled($disabled)
    {
        $this->disabled = $disabled;
        return $this;
    }

    public function doInsert($doInsert)
    {
        $this->doInsert = $doInsert;
        return $this;
    }

    public function isGroup($isGroup)
    {
        $this->isGroup = $isGroup;
        return $this;
    }

    function addChild($name, $c){
        $this->children[$name] = $c;
        return $this;
    }

    function children($c){
        $this->children=$c;
        return $this;
    }

    public function doRender()
    {
        return $this->doRender;
    }

    public function isRow($i)
    {
        $this->isRow = $i;
        return $this;
    }

    public function lgCols($lgCols)
    {
        $this->lgCols = $lgCols;
        return $this;
    }

    public function valueField($valueField)
    {
        $this->valueField = $valueField;
        return $this;
    }



}
