<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class ElfinderFileInput extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/elfinder-file", ['item' => $this]);
    }
    function renderView() {
        return \View::make("admin-ui::form/view-elfinder-file", ['item' => $this]);
    }

}