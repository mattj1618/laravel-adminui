<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Request;
use Input;
use Log;
use Image;
class ImageInput extends FormInput {

    var $filenameField = null;
    var $filename = '';
    var $width=1024;
    var $height=null;
    var $useOriginalName=false;

    public function useOriginalName($useOriginalName)
    {
        $this->useOriginalName = $useOriginalName;
        return $this;
    }


    function filename($fn){
        $this->filename=$fn;
        return $this;
    }

    public function filenameField($filenameField)
    {
        $this->filenameField = $filenameField;
        return $this;
    }


    function insert($row, $value){

        $file = Request::file("$this->id");
        if(isset($file)) {
            Log::info(json_encode($file));
            if($this->useOriginalName)
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),$file->getClientOriginalName());
            else
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),str_random(10).'.'.$file->getClientOriginalExtension());
            $this->resize($url);
            parent::insert($row, $url);
            if(isset($this->filenameField))
                $row->{$this->filenameField} = $file->getClientOriginalName();
        }
        return $row;
    }

    function resize($url){
//        Image::make($url)->resize($this->width, $this->height,function ($constraint) {
//            $constraint->aspectRatio();
//            $constraint->upsize();
//        })->save();

    }

    function update($row, $value){
        Log::info($value);
        $file = Request::file("$this->id");
        if(isset($file)) {
            Log::info(json_encode($file));
            if($this->useOriginalName)
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),$file->getClientOriginalName());
            else
                $url = $file->move(Config::get('admin-ui.file-upload-dir'),str_random(10).'.'.$file->getClientOriginalExtension());
            $this->resize($url);
            parent::insert($row, $url);
            if(isset($this->filenameField))
                $row->{$this->filenameField} = $file->getClientOriginalName();
        }
        return $row;
    }

    function render()
    {
        return View::make("admin-ui::form/image", ['item' => $this]);
    }


    function renderView() {
        return \View::make("admin-ui::form/view-image", ['item' => $this]);
    }

}