<?php namespace MJ1618\AdminUI\Form;



class RadioSelect extends FormInput {

    var $rows;
    var $idField;
    var $nameField;

    public function render(){
        return \View::make("admin-ui::form/radio", ['item' => $this]);
    }

    function renderView(){

        $dd = clone $this;
        foreach($this->rows as $row){
            if($this->isDefault($row[$this->idField])){
                $dd->defaultValue($row[$this->nameField]);
                break;
            }
        }

        \Debugbar::info($dd->defaultValue);

        return \View::make("admin-ui::form/view", ['item' => $dd]);
    }

    function insert($row, $value){

        if($value==='none'){
            parent::insert($row, null);
        } else {
            parent::insert($row, $value);
        }

        return $row;
    }

    function update($row, $value){
        if($value==='none'){
            parent::update($row, null);
        } else {
            parent::update($row, $value);
        }
        return $row;
    }

    public function isDefault($id){
        return $this->defaultValue!=='' && $this->defaultValue == $id;
    }


    public function rows($rows)
    {
        $this->rows = $rows;
        return $this;
    }

    public function idField($idField)
    {
        $this->idField = $idField;
        return $this;
    }

    public function nameField($nameField)
    {
        $this->nameField = $nameField;
        return $this;
    }


//    List<Row> rows = new ArrayList<>();
//    boolean nullable=true;
//    String id;
//    String idField;
//    String nameField;
//    String defaultValue="";
//    String label;
//
//    public DropDown(){}
//
//    @Override
//    public String render() {
//        Map<String,Object> model = new HashMap<String, Object>();
//
//        if(nullable){
//            Row row = new Row();
//            row.put(idField,"");
//            row.put(nameField,"None");
//            rows.add(0,row);
//        }
//
//        model.put("dropdown",this);
//        return Velocity.engine.render(new ModelAndView(model, "/assets/vms/form/dropdown.vm"));
//    }
//
//    @Override
//    public Object parse(String s){
//        try{
//            return Integer.parseInt(s);
//        } catch(Exception e){
//            return null;
//        }
//    }
//
//    public String defaultText(){
//        Optional<Row> d = rows.stream().filter(r->r!=null && r.asString(id).equals(defaultValue)).findFirst();
//        if(d.isPresent())return d.get().asString(nameField);
//        else return "None";
//    }



}
