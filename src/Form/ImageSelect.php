<?php namespace MJ1618\AdminUI\Form;



class ImageSelect extends FormInput {

    var $rows;
    var $row;
    var $nullable=true;
    var $idField;
    var $nameField;
    var $printValue;

    public function printValue($printValue)
    {
        $this->printValue = $printValue;
        return $this;
    }



    public function render(){
        return \View::make("admin-ui::form/imageselect", ['item' => $this]);
    }

    function renderView(){

        $dd = clone $this;

        if(!isset($this->printValue)){

            $hasValue = false;
            foreach($this->rows as $row){
                if($this->isDefault($row[$this->idField])){
                    $dd->defaultValue($row[$this->nameField]);
                    $dd->row = $row->toArray();
//                    dd($dd->row);

                    $hasValue=true;
                    break;
                }
            }
            if($hasValue===false || $dd->defaultValue===''){
                $dd->defaultValue('None');
            }
            \Debugbar::info($dd->defaultValue);

        } else {
            $dd->defaultValue($this->printValue);
        }

        return \View::make("admin-ui::form/view-imageselect", ['item' => $dd]);
    }

    function insert($row, $value){

        if($value==='none'){
            parent::insert($row,  null);
        } else {
            parent::insert($row, $value);
        }

        return $row;
    }

    function update($row, $value){
        if($value==='none'){
            parent::update($row, null);
        } else {
            parent::update($row,  $value);
        }
        return $row;
    }

    public function isDefault($id){
        return $this->defaultValue!=='' && $this->defaultValue == $id;
    }

    public function nullable($nullable)
    {
        $this->nullable = $nullable;
        return $this;
    }

    public function rows($rows)
    {
        $this->rows = $rows;
        return $this;
    }

    public function idField($idField)
    {
        $this->idField = $idField;
        return $this;
    }

    public function nameField($nameField)
    {
        $this->nameField = $nameField;
        return $this;
    }


//    List<Row> rows = new ArrayList<>();
//    boolean nullable=true;
//    String id;
//    String idField;
//    String nameField;
//    String defaultValue="";
//    String label;
//
//    public DropDown(){}
//
//    @Override
//    public String render() {
//        Map<String,Object> model = new HashMap<String, Object>();
//
//        if(nullable){
//            Row row = new Row();
//            row.put(idField,"");
//            row.put(nameField,"None");
//            rows.add(0,row);
//        }
//
//        model.put("dropdown",this);
//        return Velocity.engine.render(new ModelAndView(model, "/assets/vms/form/dropdown.vm"));
//    }
//
//    @Override
//    public Object parse(String s){
//        try{
//            return Integer.parseInt(s);
//        } catch(Exception e){
//            return null;
//        }
//    }
//
//    public String defaultText(){
//        Optional<Row> d = rows.stream().filter(r->r!=null && r.asString(id).equals(defaultValue)).findFirst();
//        if(d.isPresent())return d.get().asString(nameField);
//        else return "None";
//    }



}
