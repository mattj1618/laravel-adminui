<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

 class TextAreaBox  extends FormInput {

     var $images=null;

     function __construct(){
         $this->isRow=true;
         $this->lgCols=12;
     }

     function images($imgs){
         $images=$imgs;
         return $this;
     }

     function render()
     {
         return View::make("admin-ui::form/textarea", ['item' => $this]);
     }

 }
