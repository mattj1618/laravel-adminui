<?php namespace MJ1618\AdminUI\Form;


use DateTime;
use Illuminate\Support\Facades\View;

class DateInput extends FormInput{


    function __construct(){
        $this->defaultValue = date('d/m/Y',time());
    }
    public function render(){
        return View::make("admin-ui::form/date", ['item' => $this]);
    }



    public function defaultValue($v,$v2=null){
        if(isset($v) && $v>0)
            $this->defaultValue = date('d/m/Y',strtotime($v));

        return $this;
    }

    function insert($row, $value){

        parent::insert($row, ''.DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d'));
        return $row;
    }

    function update($row, $value){
        parent::update($row, ''.DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d'));
        return $row;
    }

//
//    String id;
//    String label;
//    String defaultText = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
//
//
//    @Override
//    public String render() {
//
//        Map<String,Object> model = new HashMap<String, Object>();
//        model.put("date",this);
//        return Velocity.engine.render(new ModelAndView(model, "/assets/vms/form/date.vm"));
//    }
//    @Override
//    public Object parse(String s) {
//        try {
//            return new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(s).getTime());
//        } catch (ParseException e) {
//            return null;
//        }
//    }
//
//    public String id() {
//        return this.id;
//    }
//
//    public String label() {
//        return this.label;
//    }
//
//    public String defaultText() {
//        return this.defaultText;
//    }
//
//    public DateInput id(final String id) {
//        this.id = id;
//        return this;
//    }
//
//    public DateInput label(final String label) {
//        this.label = label;
//        return this;
//    }
//
//    public DateInput defaultText(final String defaultText) {
//        this.defaultText = defaultText;
//        return this;
//    }
//
//
//    public DateInput defaultDate(Row row) {
//        if(row!=null&&row.contains(id))
//            this.defaultText=new SimpleDateFormat("dd/MM/yyyy").format(new Date(row.date(id).getTime()));
//        return this;
//    }
}
