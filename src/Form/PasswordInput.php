<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;
class PasswordInput extends FormInput{

    var $placeHolder='Password';

    public function placeHolder($placeHolder)
    {
        $this->placeHolder = $placeHolder;
        return $this;
    }



    function render() {
        return View::make("admin-ui::form/password", ['item' => $this]);
    }
    function renderView() {

        $item = clone $this;
        $item->defaultValue=$this->placeHolder;

        return View::make("admin-ui::form/view", ['item' => $item]);
    }

    public function parse($s){
        if($s==null||$s==''||$s===$this->placeHolder)
            return $this->defaultValue;
        else
            return $s;
    }

}
