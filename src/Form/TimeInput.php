<?php namespace MJ1618\AdminUI\Form;


use DateTime;
use Illuminate\Support\Facades\View;

class TimeInput extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/time", ['item' => $this]);
    }

    function parse($s){

        return DateTime::createFromFormat('h:i A', $s)->format('H:i:00');

    }

}