<?php namespace MJ1618\AdminUI\Form;


use DateTime;
use Exception;
use Illuminate\Support\Facades\View;
use Log;
class DobInput extends FormInput{

    public function render(){

        $dr = clone $this;

        try{
            $dt = DateTime::createFromFormat('Y-m-d', $this->defaultValue);
            if($dt!==false)
                $dr->defaultValue = $dt->format('d/m/Y');
            else
                $dr->defaultValue=null;
        } catch(Exception $e){
            $dr->defaultValue=null;
            Log::error($e->getMessage());
        }

        return View::make("admin-ui::form/dob", ['item' => $dr]);
    }

    public function renderView(){
        $dr = clone $this;
        try{
            $dt = DateTime::createFromFormat('Y-m-d', $this->defaultValue);
            if($dt!==false)
                $dr->defaultValue = $dt->format('d/m/Y');
            else
                $dr->defaultValue=null;
        } catch(Exception $e){
            $dr->defaultValue=null;
            Log::error($e->getMessage());
        }

        return View::make("admin-ui::form/view", ['item' => $dr]);
    }
    function parse($s){
        try{
            $dt = DateTime::createFromFormat('d/m/Y', $s);
            if($dt!==false)
                $this->defaultValue = $dt->format('Y-m-d');
            else
                $this->defaultValue=null;

        } catch(Exception $e){
            Log::error($e->getMessage());
        }
        return $this->defaultValue;
    }

}
