<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class TextOutputItem extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/textoutput", ['item' => $this]);
    }

}