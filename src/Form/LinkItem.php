<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class LinkItem extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/link", ['item' => $this]);
    }

}