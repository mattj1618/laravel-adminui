<?php namespace MJ1618\AdminUI\Form;


use DateTime;
use Illuminate\Support\Facades\View;

class DateRange extends FormInput{

    var $idStart;
    var $idEnd;
    var $startDate;
    var $endDate;

    function __construct(){
        $this->defaultValue = date('Y-m-d',time());
    }

    function getDates($defVal){
        $value =  explode(" - ",$defVal);
        $start = DateTime::createFromFormat('F j, Y', $value[0]);
        $end = DateTime::createFromFormat('F j, Y', $value[1]);
        return [$start,$end];
    }

    public function render(){
        $value =  explode(" - ",$this->defaultValue);

        if(count($value)===2) {
            $start = DateTime::createFromFormat('Y-m-d', $value[0]);
            if ($start instanceof DateTime) {
                $this->startDate = $start->format('F j, Y');
            }

            $end = DateTime::createFromFormat('Y-m-d', $value[1]);
            if ($end instanceof DateTime) {
                $this->endDate = $end->format('F j, Y');
            }
        } else {
            $this->startDate = (new DateTime('now'))->format('F j, Y');
            $this->endDate = (new DateTime('now'))->format('F j, Y');
        }
        return View::make("admin-ui::form/daterange", ['item' => $this]);
    }

    public function renderView(){

        $dr = clone $this;

        $value =  explode(" - ",$this->defaultValue);
        $start = ''.DateTime::createFromFormat('Y-m-d', $value[0])->format('F j, Y');
        $end = ''.DateTime::createFromFormat('Y-m-d', $value[1])->format('F j, Y');
        $dr->defaultValue="$start - $end";
        return View::make("admin-ui::form/view", ['item' => $dr]);
    }




    function insert($row, $v){
        $value = explode(" - ",$v);
        $row->{$this->idStart} = ''.DateTime::createFromFormat('F j, Y', $value[0])->format('Y-m-d');
        $row->{$this->idEnd} = ''.DateTime::createFromFormat('F j, Y', $value[1])->format('Y-m-d');
    }

    function update($row, $v){
        $value = explode(" - ",$v);
        $row->{$this->idStart} = ''.DateTime::createFromFormat('F j, Y', $value[0])->format('Y-m-d');
        $row->{$this->idEnd} = ''.DateTime::createFromFormat('F j, Y', $value[1])->format('Y-m-d');
    }

    public function idStart($idStart)
    {
        $this->idStart = $idStart;
        return $this;
    }

    public function idEnd($idEnd)
    {
        $this->idEnd = $idEnd;
        return $this;
    }

}
