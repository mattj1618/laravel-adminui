<?php namespace MJ1618\AdminUI\Form;


use Illuminate\Support\Facades\View;

class SubmitItem extends FormInput {

    function render()
    {
        return View::make("admin-ui::form/submit", ['item' => $this]);
    }

}